package com.hirely.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.hirely.activity.MainActivity;
import com.hirely.receiver.GcmBroadcastReceiver;
import com.hirely.util.PrefUtil;
import com.hirelyprovider.app.R;

public class GcmIntentService extends IntentService {

    public static final int NOTIFICATION_ID = 2102016;
    private static final String TAG = GcmIntentService.class.getSimpleName();
    public static String BROADCAST_ACTION = "com.hirely.SHOW_DIALOG";

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        String verification = PrefUtil.getProviderVerification(this);

        if (!extras.isEmpty() && verification != null && verification.equals("1")) {

            Log.i(TAG, "Received: " + extras.toString());

            String key = extras.getString("message");
            if (key != null) {
                Intent i = new Intent(BROADCAST_ACTION);
                i.putExtra("type", key);
                sendNotificationProvider(key);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
            }

        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotificationProvider(String key) {

        if (PrefUtil.getAppInBackround(this)) {

            String text = "";
            switch (key) {
                case "NEW_JOB":
                    text = "New Job For You";
                    break;
                case "JOB_CANCELED":
                    text = "Job was canceled";
                    break;
            }

            NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Hirely Provider")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                    .setContentText(text);
            builder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, builder.build());
            PrefUtil.setLastNotificationID(this, NOTIFICATION_ID);
        }
    }
}
