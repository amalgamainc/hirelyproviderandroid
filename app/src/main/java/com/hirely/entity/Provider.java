package com.hirely.entity;

import android.util.Log;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class Provider implements Serializable {

    private static final String TAG = Provider.class.getSimpleName();

    public String id;
    public String name;
    public String company;
    private Map<String, Integer> servicePrices = new HashMap<>();

    public boolean isServicePricesEmpty() {
        return servicePrices.isEmpty();
    }

    public int getServicePrice (String serviceName) {
        return servicePrices.get(serviceName);
    }

    public Set<String> getServiceSet () {
        return servicePrices.keySet();
    }

    public void addAllServicePrices (Map<String, Integer> data) {
        servicePrices.putAll(data);
    }

    public boolean doesMakeService (String serviceName) {
        return servicePrices.containsKey(serviceName);
    }

    public boolean doesMakeAllServices (Set<String> serviceNames) {

        Log.d("MassageTest", TAG +  serviceNames.toString());
        return servicePrices.keySet().containsAll(serviceNames);
    }

    public void addServicePriceIfNotNull (String serviceName, int servicePrice) {
        if (servicePrice > 0) {
            servicePrices.put(serviceName, servicePrice);
        }
    }

    public int serviceTotalCost (Set<String> serviceNames) {
        int totalCost = 0;

        for (String serviceName: serviceNames) {
            totalCost += servicePrices.get(serviceName);
        }
        return totalCost;
    }

    public int addServCost (Set<String> serviceNames, String massageType) {
        int totalCost = 0;
        for (String serviceName: serviceNames) {
            if (serviceName.equalsIgnoreCase(massageType)){
                continue;
            }
            totalCost += servicePrices.get(serviceName);
        }

        return totalCost;
    }


    @Override
    public String toString() {
        return "Provider{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", company='" + company + '\''  +
                '}';
    }

}
