package com.hirely.entity;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Job implements Serializable{
    public String jobId;
    public String userId;
    public String userName;
    public String providerId;
    public String serviceId;
    public String address;
    public String distance;
    public String latitude;
    public String longitude;
    public String massageType;
    public Set<String> requestedServices = new HashSet<>();

    public String getRequestedServicesAsString (String divider) {
        StringBuilder st = new StringBuilder(60);
        for (String s: requestedServices) {
            st.append(s).append(divider);
        }
        return st.toString().trim();
    }

    @Override
    public String toString() {
        return "Job{" +
                "jobId='" + jobId + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", providerId='" + providerId + '\'' +
                ", serviceId='" + serviceId + '\'' +
                ", address='" + address + '\'' +
                ", distance='" + distance + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", massageType='" + massageType + '\'' +
                ", requestedServices='" + requestedServices + '\'' +
                '}';
    }
}
