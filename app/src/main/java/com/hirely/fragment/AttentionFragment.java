package com.hirely.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.activity.MainActivity;
import com.hirely.util.PrefUtil;
import com.hirely.util.WebServiceHandler;
import com.hirelyprovider.app.R;

public class AttentionFragment extends Fragment {

    private TextView messageText;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attention, container, false);

        messageText = (TextView) view.findViewById(R.id.attention_fr_message);
        view.findViewById(R.id.attention_fr_retry_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CheckProviderVerificationAsync(getActivity()).execute();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        new CheckProviderVerificationAsync(getActivity()).execute();
    }

    class CheckProviderVerificationAsync extends AsyncTask<Void, Void, Void> {

        private Context context;

        CheckProviderVerificationAsync(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, "", "Loading...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            WebServiceHandler.providerInformation(context);
            return null;
        }

        @Override
        protected void onPostExecute(Void s) {
            progressDialog.dismiss();
            switch (PrefUtil.getProviderVerification(context)) {
                case "0":
                    messageText.setText("Your application is being processed, please wait for a representative to give you a call");
                    break;
                case "1":
                    startActivity(new Intent(context, MainActivity.class));
                    getActivity().finish();
                    break;
                default:
                    Toast.makeText(
                            context,
                            "User verified status: " + PrefUtil.getProviderVerification(context),
                            Toast.LENGTH_LONG).show();
            }
        }
    }
}
