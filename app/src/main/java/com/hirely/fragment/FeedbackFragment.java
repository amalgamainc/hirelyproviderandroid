package com.hirely.fragment;


import android.annotation.SuppressLint;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.GlobalConstants;
import com.hirely.activity.MainActivity;
import com.hirely.entity.Job;
import com.hirely.util.PrefUtil;
import com.hirely.util.WebServiceHandler;
import com.hirelyprovider.app.R;

@SuppressLint("NewApi")
public class FeedbackFragment extends Fragment {

    private Button submitFeedback;
    private RatingBar ratingBar;
    private EditText commentEdt;
    private TextView clientName;
    private TextView priceForWork;
    private TextView earnedToday;


    class SendFeedbackTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            String res = WebServiceHandler.sendFeedback (
                    getActivity(),
                    PrefUtil.getJobServiceId(getActivity()),
                    getComment(),
                    getRating()
            );

            if (res.equalsIgnoreCase(WebServiceHandler.SUCCESS)) {
                res = WebServiceHandler.updateProviderStatus(getActivity(), GlobalConstants.PROVIDER_STATUS_0_WAIT_FOR_NEW_JOB);
            }
            return res;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equals("1")) {
                getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
            }
        }
    }


    class GetMoneyInfoTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.getMoneyForDayAndTotal(getActivity());
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equalsIgnoreCase(WebServiceHandler.SUCCESS)) {
                earnedToday.setText(PrefUtil.getEarnedToday(getActivity()) + "PHP");
            } else {
                Toast.makeText(getActivity(), "Error when try to get money", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        clientName = (TextView) view.findViewById(R.id.username);
        clientName.setText(PrefUtil.getJobUserUsername(getActivity()));
        priceForWork = (TextView) view.findViewById(R.id.priceForWork);
        earnedToday = (TextView) view.findViewById(R.id.earnedToday);
        ratingBar = (RatingBar) view.findViewById(R.id.rating);
        commentEdt = (EditText) view.findViewById(R.id.feedback_fr_comment);

        submitFeedback = (Button) view.findViewById(R.id.feedback_fr_submit_btn);
        submitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendFeedbackTask().execute();
            }
        });

        new GetMoneyInfoTask().execute();
        priceForWork.setText(PrefUtil.getJobCost(getActivity()) + " PHP");
    }

    private float getRating() {
        return ratingBar.getRating();
    }

    private String getComment() {
        return commentEdt.getText().toString();
    }


}
