package com.hirely.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.hirely.GlobalConstants;
import com.hirely.SessionManager;
import com.hirely.activity.LogInActivity;
import com.hirely.dialog.DisabledRingCaptchaDialog;
import com.hirely.util.PrefUtil;
import com.hirely.util.RingCaptchaUtil;
import com.hirely.util.ValidationUtil;
import com.hirely.util.WebServiceHandler;
import com.hirelyprovider.app.R;
import com.thrivecom.ringcaptcha.RingcaptchaAPIController;
import com.thrivecom.ringcaptcha.lib.handlers.RingcaptchaHandler;
import com.thrivecom.ringcaptcha.lib.models.RingcaptchaResponse;

public class SignUpVerifyPhoneFragment extends Fragment {

    // GENERAL
    private static final String TAG = SignUpVerifyPhoneFragment.class.getSimpleName();
    private Context context;
    // UI
    private LinearLayout sendLayout;
    private LinearLayout verifyLayout;
    private EditText phoneNumberEdt;
    private EditText companyNameEdt;
    private Spinner countryCodeSpnr;
    private EditText verificationCodeEdt;
    private Button verifyBtn;
    private Button sendVerCodeBtn;
    private ProgressDialog progressDialog;
    // FIELDS
    private String companyName;
    private String providerName;
    private String providerEmail;
    private String password;
    private String providerPhone;
    private String countryCode;
    private String mSocialLoginType;
    private Boolean mIsUpdate;


    class SignUpProviderViaFacebook extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, "", "Loading...");
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.loginFacebook(
                    context,
                    PrefUtil.getFacebookName(context),
                    PrefUtil.getFacebookEmail(context),
                    PrefUtil.getFacebookId(context),
                    PrefUtil.getDeviceID(context),
                    "A",
                    String.valueOf(PrefUtil.getProviderLatitude(context)),
                    String.valueOf(PrefUtil.getProviderLongitude(context)),
                    GlobalConstants.PROVIDER_STATUS_0_WAIT_FOR_NEW_JOB,
                    PrefUtil.getProviderServiceType(context),
                    countryCode,
                    providerPhone,
                    companyName
            );
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progressDialog.dismiss();
            if (res.equalsIgnoreCase("true")) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.sign_up_act_content_frame, new AttentionFragment())
                        .commit();
                PrefUtil.setFacebookStatus(context, true);

                Log.e("Facebook Login ", "SUCCESS");
            } else {
                Log.e("Facebook Login ", "FAILED");
                Toast.makeText(context, "Username or Password is Incorrect", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class SignUpProviderViaGooglePlus extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, "", "Loading...");
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.loginGooglePlus(
                    context,
                    PrefUtil.getGoogleName(context),
                    PrefUtil.getGoogleEmail(context),
                    PrefUtil.getGoogleId(context),
                    PrefUtil.getDeviceID(context),
                    "A",
                    String.valueOf(PrefUtil.getProviderLatitude(context)),
                    String.valueOf(PrefUtil.getProviderLongitude(context)),
                    GlobalConstants.PROVIDER_STATUS_0_WAIT_FOR_NEW_JOB,
                    PrefUtil.getProviderServiceType(context),
                    countryCode,
                    providerPhone,
                    companyName
            );
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progressDialog.dismiss();

            if (res.equalsIgnoreCase("true")) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.sign_up_act_content_frame, new AttentionFragment())
                        .commit();
                PrefUtil.setGoogleStatus(context, true);

                Log.e("Google Plus Login ", "SUCCESS");
            } else {
                Log.e("Google PLus Login ", "FAILED");
                Toast.makeText(context, "Username or Password is Incorrect", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class EditProfileTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, "", "Changing account...");
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.editProviderProfile(getActivity(), companyName, providerName, providerEmail, providerPhone, countryCode);
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progressDialog.dismiss();

            if (res.equalsIgnoreCase(WebServiceHandler.SUCCESS)) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new ProfileFragment()).commit();
                Toast.makeText(getActivity(), "Changes applied", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Something's wrong", Toast.LENGTH_LONG).show();
            }
        }
    }

    class SignUpProviderTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, "", "Creating account...");
        }

        @Override
        protected String doInBackground(Void... voids) {

            return WebServiceHandler.createProviderAccount(
                    context,
                    companyName,
                    providerName,
                    password,
                    providerEmail,
                    countryCode,
                    providerPhone,
                    PrefUtil.getDeviceID(context),
                    "A",
                    String.valueOf(PrefUtil.getProviderLatitude(context)),
                    String.valueOf(PrefUtil.getProviderLongitude(context)),
                    PrefUtil.getProviderServiceType(context),
                    GlobalConstants.PROVIDER_STATUS_0_WAIT_FOR_NEW_JOB
            );
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progressDialog.dismiss();
            if (res.equalsIgnoreCase(WebServiceHandler.SUCCESS)) {
                SessionManager.createLoginSession(context, providerEmail, password);
                getFragmentManager().beginTransaction().replace(R.id.sign_up_act_content_frame, new AttentionFragment()).commit();
            } else {
                Toast.makeText(context, "Provider already exist", Toast.LENGTH_LONG).show();
                getFragmentManager().beginTransaction().replace(R.id.sign_up_act_content_frame, new SignUpMainFormFragment()).commit();
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_verify_phone, container, false);

        context = getActivity();

        sendLayout = (LinearLayout) view.findViewById(R.id.verify_phone_fr_send_code_view);
        countryCodeSpnr = (Spinner) view.findViewById(R.id.verify_phone_fr_country_code_spnr);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.country_codes, R.layout.custom_spinner_item);
        countryCodeSpnr.setAdapter(adapter);
        phoneNumberEdt = (EditText) view.findViewById(R.id.verify_phone_fr_phone_edt);
        companyNameEdt = (EditText) view.findViewById(R.id.verify_phone_fr_company);

        sendVerCodeBtn = (Button) view.findViewById(R.id.verify_phone_fr_send_code_btn);
        sendVerCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ValidationUtil.isMobilePhoneValid(phoneNumberEdt.getText().toString())) {
                    providerPhone = phoneNumberEdt.getText().toString();
                    countryCode = countryCodeSpnr.getSelectedItem().toString().split(", ")[1];
                    if (companyName == null || companyName.isEmpty()) {
                        companyName = companyNameEdt.getText().toString();
                    }

                    //with Capcha----------
                    RingCaptchaUtil.getInstance().sendSMSWithCode(context, countryCode + providerPhone, new RingcaptchaHandler() {
                        @Override
                        public void onSuccess(RingcaptchaResponse ringcaptchaResponse) {
                            if (ringcaptchaResponse.status.equalsIgnoreCase("ERROR")) {

                                new DisabledRingCaptchaDialog(getContext(), ringcaptchaResponse.message) {
                                    @Override
                                    public void goBack() {
                                        getActivity().finish();
                                    }
                                }.show();
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e(TAG, e.getLocalizedMessage(), e);
                        }
                    });
                    //------------------


                    sendLayout.setVisibility(View.GONE);
                    verifyLayout.setVisibility(View.VISIBLE);
                } else {
                    phoneNumberEdt.setError("Wrong phone");
                }
            }
        });

        verifyLayout = (LinearLayout) view.findViewById(R.id.verify_phone_fr_verify_view);
        verificationCodeEdt = (EditText) view.findViewById(R.id.verify_phone_fr_code_edt);
        verifyBtn = (Button) view.findViewById(R.id.verify_phone_fr_verify_btn);
        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //with Captcha -----------------------------------------
                RingCaptchaUtil.getInstance().checkCodeFromSMS(context, new RingcaptchaHandler() {

                    @Override
                    public void onSuccess(RingcaptchaResponse o) {
                        Log.e(TAG, "verifyCaptchaWithCode " + o.toString());
                        Toast.makeText(context, "Verfied", Toast.LENGTH_LONG).show();
                        RingcaptchaAPIController.setSMSHandler(null);
                        if (mSocialLoginType == null) {
                            if (mIsUpdate) {
                                //new Thread(editProfile).start();
                                new EditProfileTask().execute();
                            } else {
                                new SignUpProviderTask().execute();
                            }
                        } else {
                            if (mSocialLoginType.equalsIgnoreCase("googleplus")) {
                                //progressDialog = ProgressDialog.show(context, "", "Loading...");
                                //new Thread(threadGooglePlusLogin).start();
                                new SignUpProviderViaGooglePlus().execute();
                            } else if (mSocialLoginType.equalsIgnoreCase("facebook")) {
//                                progressDialog = ProgressDialog.show(context, "", "Loading...");
//                                new Thread(threadFacebookLogin).start();
                                new SignUpProviderViaFacebook().execute();
                            }
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e(TAG, e.getLocalizedMessage(), e);
                        verificationCodeEdt.setError(e.getLocalizedMessage());
                    }
                }, verificationCodeEdt.getText().toString());
                //-------------------------------------

            }
        });

        mSocialLoginType = PrefUtil.getSocialLoginType(context);
        if (getArguments() == null) {
            verifyLayout.setVisibility(View.GONE);
            sendLayout.setVisibility(View.VISIBLE);
        } else {
            sendLayout.setVisibility(View.GONE);
            verifyLayout.setVisibility(View.VISIBLE);

            Log.e(TAG, getArguments().toString());
            Bundle bundle = getArguments();

            mIsUpdate = bundle.getBoolean("isUpdate", false);
            companyName = bundle.getString("companyName");
            if (companyNameEdt != null) {
                companyNameEdt.setText(companyName);
            }
            providerName = bundle.getString("providerName");
            providerEmail = bundle.getString("email");
            password = bundle.getString("password");
            providerPhone = bundle.getString("phoneNumber");
            countryCode = bundle.getString("countryCode");

            //with Captcha------------
            RingCaptchaUtil.getInstance().sendSMSWithCode(context, countryCode + providerPhone, new RingcaptchaHandler() {
                @Override
                public void onSuccess(RingcaptchaResponse ringcaptchaResponse) {
                    if (ringcaptchaResponse.status.equalsIgnoreCase("ERROR")) {

                        new DisabledRingCaptchaDialog(getContext(), ringcaptchaResponse.message) {
                            @Override
                            public void goBack() {
                                getActivity().finish();
                            }
                        }.show();
                    }
                }

                @Override
                public void onError(Exception e) {
                    Log.e(TAG, e.getLocalizedMessage(), e);

                }
            });
            //--------------------------
        }

        return view;
    }
}
