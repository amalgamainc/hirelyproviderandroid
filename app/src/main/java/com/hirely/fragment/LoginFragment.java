package com.hirely.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.hirely.GlobalConstants;
import com.hirely.SessionManager;
import com.hirely.activity.ForgetPasswordActivity;
import com.hirely.activity.MainActivity;
import com.hirely.activity.SignUpActivity;
import com.hirely.service.GPSTrackerService;
import com.hirely.util.PrefUtil;
import com.hirely.util.ValidationUtil;
import com.hirely.util.WebServiceHandler;
import com.hirelyprovider.app.R;

import org.brickred.socialauth.Profile;
import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthListener;

import java.io.IOException;

public class LogInFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = LogInFragment.class.getSimpleName();

    Button logInBtn;
    Button signUpBtn;
    Button fbLogInBtn, gplusLogInBtn;
    TextView forgetTxt;
    EditText emailEdt, passwordEdt;
    ProgressDialog progressDialog;
    SessionManager session;
    CheckBox rememberCheckbox;

    String emailStr, passwordStr;
    // SOCIAL LOGIN
    SocialAuthAdapter adapter;
    String token;
    Profile profileMap;
    String providerName, providerEmail, providerId, deviceId, providerLatitude, providerLongitude, providerServiceType;
    // GCM
    String GCM_SENDER_ID = "542173807799";
    GoogleCloudMessaging gcm;


    class LogInTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.loginWithHirely(
                        getActivity(),
                        emailStr,
                        passwordStr,
                        PrefUtil.getDeviceID(getActivity()),
                        "A",
                        String.valueOf(PrefUtil.getProviderLatitude(getActivity())),
                        String.valueOf(PrefUtil.getProviderLongitude(getActivity())),
                        "P"
            );
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progressDialog.dismiss();
            if (res.equalsIgnoreCase(WebServiceHandler.SUCCESS)) {
                if (PrefUtil.getProviderVerification(getActivity()).equals("1")) {
                    startActivity(new Intent(getActivity(), MainActivity.class));
                } else {
                    getFragmentManager().beginTransaction().replace(R.id.frame_container, new AttentionFragment()).commit();
                }
            } else {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        adapter = new SocialAuthAdapter(new ResponseListener());

        GPSTrackerService.updateLocationInPreferences(getActivity());

        gcm = GoogleCloudMessaging.getInstance(getActivity());
        registerOnGCMInBackground();

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailEdt = (EditText) view.findViewById(R.id.log_in_fr_provider_email_edt);
        passwordEdt = (EditText) view.findViewById(R.id.log_in_fr_password_edt);
        logInBtn = (Button) view.findViewById(R.id.log_in_fr_log_in_btn);
        signUpBtn = (Button) view.findViewById(R.id.log_in_fr_sign_up_btn);
        fbLogInBtn = (Button) view.findViewById(R.id.log_in_fr_log_in_facebook_btn);
        gplusLogInBtn = (Button) view.findViewById(R.id.log_in_fr_log_in_gplus_btn);
        forgetTxt = (TextView) view.findViewById(R.id.log_in_fr_forgot_txt);
        rememberCheckbox = (CheckBox) view.findViewById(R.id.log_in_fr_remember_check);

        logInBtn.setOnClickListener(this);
        forgetTxt.setOnClickListener(this);
        signUpBtn.setOnClickListener(this);
        fbLogInBtn.setOnClickListener(this);
        gplusLogInBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        final int viewId = v.getId();

        switch (viewId) {
            case R.id.log_in_fr_sign_up_btn:
                startActivity(new Intent(getActivity(), SignUpActivity.class));
                break;

            case R.id.log_in_fr_forgot_txt:
                startActivity(new Intent(getActivity(), ForgetPasswordActivity.class));
                break;

            case R.id.log_in_fr_log_in_facebook_btn:
                PrefUtil.setSocialLoginType(getActivity(), "facebook");

                if (PrefUtil.getFacebookStatus(getActivity())) {
                    startActivity(new Intent(getActivity(), MainActivity.class));
                } else {
                    adapter.authorize(getActivity(), SocialAuthAdapter.Provider.FACEBOOK);
                    adapter.enable(fbLogInBtn);
                    adapter.signOut(getActivity(), SocialAuthAdapter.Provider.FACEBOOK.toString());
                }
                break;

            case R.id.log_in_fr_log_in_gplus_btn:
                PrefUtil.setSocialLoginType(getActivity(), "googleplus");
                if(PrefUtil.getGoogleStatus(getActivity())) {
                    startActivity(new Intent(getActivity(), MainActivity.class));
                } else {
                    adapter.authorize(getActivity(), SocialAuthAdapter.Provider.GOOGLEPLUS);
                    adapter.enable(fbLogInBtn);
                    adapter.signOut(getActivity(), SocialAuthAdapter.Provider.GOOGLEPLUS.toString());
                }
                break;

            case R.id.log_in_fr_log_in_btn:
                emailStr = emailEdt.getText().toString().trim();
                passwordStr = passwordEdt.getText().toString().trim();

                if (isValid(emailStr, passwordStr)) {
                    if (rememberCheckbox.isChecked()) {
                        SessionManager.createLoginSession(getActivity(), emailStr, passwordStr);
                    }
                    new LogInTask().execute();
                }
                break;

            default:
                break;
        }


        if (viewId == R.id.log_in_fr_sign_up_btn) {
            startActivity(new Intent(getActivity(), SignUpActivity.class));
        } else if (viewId == R.id.log_in_fr_forgot_txt) {
            startActivity(new Intent(getActivity(), ForgetPasswordActivity.class));
        } else if (viewId == R.id.log_in_fr_log_in_facebook_btn) {
            PrefUtil.setSocialLoginType(getActivity(), "facebook");
            Boolean boolValue = PrefUtil.getFacebookStatus(getActivity());
            if (!boolValue) {
                adapter.authorize(getActivity(), SocialAuthAdapter.Provider.FACEBOOK);
                adapter.enable(fbLogInBtn);
                adapter.signOut(getActivity(), SocialAuthAdapter.Provider.FACEBOOK.toString());
            } else {
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
        } else if (viewId == R.id.log_in_fr_log_in_btn) {
            emailStr = emailEdt.getText().toString().trim();
            passwordStr = passwordEdt.getText().toString().trim();

            if (isValid(emailStr, passwordStr)) {
                if (rememberCheckbox.isChecked()) {
                    SessionManager.createLoginSession(getActivity(), emailStr, passwordStr);
                }
                new LogInTask().execute();
            }
        }
    }

    private boolean isValid(String email, String password) {
        if (!ValidationUtil.isEmailValid(email)) {
            emailEdt.setError("Invalid Username");
        } else if (password.length() < 1) {
            passwordEdt.setError("Invalid Password");
        } else {
            return true;
        }
        return false;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void registerOnGCMInBackground() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getActivity());
                    }
                    if (isAdded()) {
                        PrefUtil.setDeviceID(getActivity(), gcm.register(GCM_SENDER_ID));
                    }
                } catch (IOException ex) {
                    Log.e(TAG, ex.getLocalizedMessage(), ex);
                }
                return null;
            }
        }.execute();
    }

    public class ResponseListener implements DialogListener {
        @Override
        public void onBack() {
        }

        @Override
        public void onCancel() {
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onComplete(Bundle arg0) {
            token = adapter.getCurrentProvider().getAccessGrant().getKey();
            adapter.getUserProfileAsync(new ProfileDataListener());

            progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");
        }

        @Override
        public void onError(SocialAuthError arg0) {
        }
    }

    @SuppressWarnings("rawtypes")
    class ProfileDataListener implements SocialAuthListener {
        @Override
        public void onError(SocialAuthError arg0) {
        }

        @Override
        public void onExecute(String arg0, Object arg1) {
            Log.d("Custom-UI", "Receiving Data");

            profileMap = adapter.getUserProfile();

            providerName = profileMap.getFirstName();
            providerEmail = profileMap.getEmail();
            providerId = profileMap.getValidatedId();
            deviceId = PrefUtil.getDeviceID(getContext());
            providerLatitude = String.valueOf(PrefUtil.getProviderLatitude(getContext()));
            providerLongitude = String.valueOf(PrefUtil.getProviderLongitude(getContext()));
            providerServiceType = PrefUtil.getProviderServiceType(getContext());

            progressDialog.dismiss();

            if (profileMap.getProviderId().equalsIgnoreCase("facebook")) {
                PrefUtil.setSocialLoginType(getActivity(), "facebook");
                PrefUtil.setFacebookId(getActivity(), profileMap.getValidatedId());
                PrefUtil.setFacebookEmail(getActivity(), profileMap.getEmail());
                PrefUtil.setFacebookName(getActivity(), profileMap.getFirstName());
                new LogInFacebookTask().execute();

            } else if (profileMap.getProviderId().equalsIgnoreCase("googleplus")) {
                PrefUtil.setSocialLoginType(getActivity(), "googleplus");
                PrefUtil.setGoogleId(getActivity(), profileMap.getValidatedId());
                PrefUtil.setGoogleEmail(getActivity(), profileMap.getEmail());
                PrefUtil.setGoogleName(getActivity(), profileMap.getFirstName());
                new LogInGPlusTask().execute();
            }
        }
    }

    class LogInFacebookTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.loginFacebook(
                    getActivity(),
                    providerName,
                    providerEmail,
                    providerId,
                    deviceId,
                    "A",
                    providerLatitude,
                    providerLongitude,
                    GlobalConstants.PROVIDER_STATUS_0_WAIT_FOR_NEW_JOB,
                    providerServiceType,
                    "",
                    "",
                    ""
            );
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progressDialog.dismiss();
            if (res.equalsIgnoreCase("true")) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                Log.e("Facebook Login ", "SUCCESS");
            } else {
                Log.e("Facebbok Login ", "FAILED");
            }
        }
    }

    class LogInGPlusTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.loginGooglePlus(
                    getContext(),
                    providerName,
                    providerEmail,
                    providerId,
                    deviceId,
                    "A",
                    providerLatitude,
                    providerLongitude,
                    GlobalConstants.PROVIDER_STATUS_0_WAIT_FOR_NEW_JOB,
                    providerServiceType,
                    "",
                    "",
                    ""
            );
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progressDialog.dismiss();
            if (res.equalsIgnoreCase("true")) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                Log.e("Google Plus Login ", "SUCCESS");
            } else {
                Log.e("Google Plus Login ", "FAILED");
            }
        }
    }
}
