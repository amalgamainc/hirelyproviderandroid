package com.hirely.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.GlobalConstants;
import com.hirely.activity.MainActivity;
import com.hirely.entity.Job;
import com.hirely.service.GPSTrackerService;
import com.hirely.util.PrefUtil;
import com.hirely.util.WebServiceHandler;
import com.hirelyprovider.app.R;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@SuppressLint("NewApi")
public class ServicInProcessFragment extends Fragment {

    private static final String TAG = ServicInProcessFragment.class.getSimpleName();

    public static String timerValueStr;
    Button finishBtn, stopBtn;
    TextView timerTxt;
    TextView serviceList;
    long timeInMilliseconds = 0L;
    ProgressDialog progressDialog;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    private double priceForWork;

    class FinishJobTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");
        }

        @Override
        protected String doInBackground(String... data) {
            String res = WebServiceHandler.finishJob(getActivity(), data[0]);
            if (res.equalsIgnoreCase(WebServiceHandler.SUCCESS)){
                res = WebServiceHandler.updateProviderStatus(getActivity(), GlobalConstants.PROVIDER_STATUS_4_FEEDBACK);
            }
            return res;
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progressDialog.dismiss();
            if (res.equals("1")) {
                PrefUtil.setProviderStatus(getActivity(), GlobalConstants.PROVIDER_STATUS_4_FEEDBACK);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new FeedbackFragment()).commit();
            } else {
                Toast.makeText(getActivity(), "Can't finish job", Toast.LENGTH_LONG).show();
            }
        }
    }

    class TimerTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.TimerStop(getActivity(), timerValueStr, String.format("%.2f", priceForWork));
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equalsIgnoreCase("1")) {
                finishBtn.setVisibility(View.VISIBLE);
            }
        }
    }

    class GetServiceDetailsTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.serviceDetails(getActivity());
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progressDialog.dismiss();
            if (!res.equalsIgnoreCase(WebServiceHandler.SUCCESS)) {
                Toast.makeText(getActivity(), "" + PrefUtil.getRegisterMessage(getActivity()), Toast.LENGTH_LONG).show();
            }
        }
    }

    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            timeInMilliseconds = System.currentTimeMillis() - startTime;

            long hour = TimeUnit.MILLISECONDS.toHours(timeInMilliseconds);
            long mins = TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds) - TimeUnit.HOURS.toMinutes(hour);
            long secs = TimeUnit.MILLISECONDS.toSeconds(timeInMilliseconds) - TimeUnit.HOURS.toSeconds(hour) - TimeUnit.MINUTES.toSeconds(mins);

            priceForWork = (double) TimeUnit.MILLISECONDS.toSeconds(timeInMilliseconds) / (60 * 60) * Double.parseDouble(PrefUtil.getProviderPrice(getActivity()));
            timerTxt.setText(String.format("%d:%02d:%02d", hour, mins, secs));
            customHandler.postDelayed(this, 500);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.service_in_process_fr, container, false);

        GPSTrackerService.updateLocationInPreferences(getActivity());

        startTime = System.currentTimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);

        new GetServiceDetailsTask().execute();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        timerTxt = (TextView) view.findViewById(R.id.end_screen_fr_timer_txt);
        serviceList = (TextView) view.findViewById(R.id.end_screen_fr_service_list);
        stopBtn = (Button) view.findViewById(R.id.end_screen_fr_stop_btn);
        stopBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Press and hold to stop", Toast.LENGTH_SHORT).show();
            }
        });
        stopBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                finishBtn.setVisibility(View.VISIBLE);
                timerValueStr = timerTxt.getText().toString();
                final Handler handler = new Handler();
                new Thread(new Runnable() {
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                customHandler.removeCallbacks(updateTimerThread);
                                stopBtn.setVisibility(View.GONE);
                                new TimerTask().execute();
                            }
                        });
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                return false;
            }
        });
        finishBtn = (Button) view.findViewById(R.id.end_screen_fr_finish_btn);
        finishBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PrefUtil.setJobPrice(getActivity(), String.format("%.2f", priceForWork));
                new FinishJobTask().execute(PrefUtil.getJobCost(getActivity()));
            }
        });

        serviceList.setText(PrefUtil.getServiceList(getActivity()));

    }




}