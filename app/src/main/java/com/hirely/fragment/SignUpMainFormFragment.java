package com.hirely.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.hirely.activity.MainActivity;
import com.hirely.service.GPSTrackerService;
import com.hirely.util.PrefUtil;
import com.hirely.util.ValidationUtil;
import com.hirelyprovider.app.R;

import org.brickred.socialauth.Profile;
import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SignUpMainFormFragment extends Fragment implements View.OnClickListener {

    // GENERAL
    private static final String TAG = SignUpMainFormFragment.class.getSimpleName();
    // GCM
    String GCM_SENDER_ID = "542173807799";
    GoogleCloudMessaging gcm;
    String regId;
    // SOCIAL LOGIN
    SocialAuthAdapter socialAuthAdapter;
    String token;
    Profile profileMap;
    private Context context;
    // UI
    private EditText companyNameEdt;
    private EditText providerNameEdt;
    private EditText providerEmailEdt;
    private EditText providerPasswordEdt;
    private Spinner providerCountryCodeSpnr;
    private EditText providerPhoneEdt;
    private Button signUpBtn;
    private Button signUpViaGPlusBtn;
    private Button signUpViaFacebookBtn;
    private CheckBox rememberCheck;
    private TextView receiveUpdatesYes, receiveUpdatesNo;
    View receiveUpdatesUnderlineYes, receiveUpdatesUnderlineNo;
    private boolean receiveUpdates;
    // FIELDS
    private String providerName;
    private String providerEmail;
    private String providerPassword;
    private String providerPhone;
    private String providerCountryCode;
    private String companyName;

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();

        GPSTrackerService.updateLocationInPreferences(getActivity());

        // gcm
        gcm = GoogleCloudMessaging.getInstance(context);
        regId = getRegistrationId();
        registerInBackground();

        PrefUtil.setServiceType(context, "Massage");

        // social login
        socialAuthAdapter = new SocialAuthAdapter(new ResponseListener());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_main_form, container, false);

        companyNameEdt = (EditText) view.findViewById(R.id.sign_up_fr_company_name_edt);
        providerNameEdt = (EditText) view.findViewById(R.id.sign_up_fr_providername_edt);
        providerPasswordEdt = (EditText) view.findViewById(R.id.sign_up_fr_provider_password_edt);
        providerEmailEdt = (EditText) view.findViewById(R.id.sign_up_fr_provider_email_edt);
        providerCountryCode = getCountryCode();
        providerCountryCodeSpnr = (Spinner) view.findViewById(R.id.sign_up_fr_provider_country_code_spnr);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.country_codes, R.layout.custom_spinner_item);
        spinnerAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        providerCountryCodeSpnr.setAdapter(spinnerAdapter);
        providerCountryCodeSpnr.setSelection(getIndexOfWhichEndWith(providerCountryCode));
        providerPhoneEdt = (EditText) view.findViewById(R.id.sign_up_fr_provider_phone_edt);

        signUpBtn = (Button) view.findViewById(R.id.sign_up_fr_sign_up_btn);
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                companyName = companyNameEdt.getText().toString();
                providerName = providerNameEdt.getText().toString();
                providerPassword = providerPasswordEdt.getText().toString();
                providerEmail = providerEmailEdt.getText().toString();
                providerPhone = providerPhoneEdt.getText().toString();
                providerCountryCode = providerCountryCodeSpnr.getSelectedItem().toString().split(", ")[1];

                if (isInputsValid(companyName, providerName, providerEmail, providerPassword, providerPhone)) {

                    SignUpVerifyPhoneFragment verifyPhoneFragment = new SignUpVerifyPhoneFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("companyName", companyName);
                    bundle.putString("providerName", providerName);
                    bundle.putString("password", providerPassword);
                    bundle.putString("email", providerEmail);
                    bundle.putString("countryCode", providerCountryCode);
                    bundle.putString("phoneNumber", providerPhone);
                    verifyPhoneFragment.setArguments(bundle);
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.sign_up_act_content_frame, verifyPhoneFragment)
                            .commit();
                }
            }

        });

        signUpViaGPlusBtn = (Button) view.findViewById(R.id.sign_up_fr_sign_up_gplus);
        signUpViaGPlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefUtil.setSocialLoginType(context, "googleplus");
                if (!PrefUtil.getGoogleStatus(context)) {
                    socialAuthAdapter.authorize(context, SocialAuthAdapter.Provider.GOOGLEPLUS);
                    socialAuthAdapter.enable(signUpViaGPlusBtn);
                } else {
                    startActivity(new Intent(context, MainActivity.class));
                }
            }
        });

        signUpViaFacebookBtn = (Button) view.findViewById(R.id.sign_up_fr_sign_up_fb);
        signUpViaFacebookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefUtil.setSocialLoginType(context, "facebook");
                if (!PrefUtil.getFacebookStatus(context)) {
                    socialAuthAdapter.authorize(context, SocialAuthAdapter.Provider.FACEBOOK);
                    socialAuthAdapter.enable(signUpViaFacebookBtn);
                } else {
                    startActivity(new Intent(context, MainActivity.class));
                }
            }
        });

        rememberCheck = (CheckBox) view.findViewById(R.id.sign_up_fr_remember_check);
        receiveUpdatesYes = (TextView) view.findViewById(R.id.sign_up_fr_updates_yes_btn);
        receiveUpdatesNo = (TextView) view.findViewById(R.id.sign_up_fr_updates_no_btn);
        receiveUpdatesYes.setOnClickListener(this);
        receiveUpdatesNo.setOnClickListener(this);
        receiveUpdatesUnderlineYes = view.findViewById(R.id.sign_up_fr_updates_yes_underline);
        receiveUpdatesUnderlineYes.setVisibility(View.INVISIBLE);
        receiveUpdatesUnderlineNo = view.findViewById(R.id.sign_up_fr_updates_no_underline);
        receiveUpdatesUnderlineNo.setVisibility(View.VISIBLE);
        receiveUpdates = false;//TODO

        return view;
    }

    @Override
    public void onClick(final View view) {
        int viewId = view.getId();
        switch (viewId) {

            case R.id.sign_up_fr_updates_yes_btn:
                receiveUpdatesUnderlineYes.setVisibility(View.VISIBLE);
                receiveUpdatesUnderlineNo.setVisibility(View.INVISIBLE);
                receiveUpdates = true;
                break;

            case R.id.sign_up_fr_updates_no_btn:
                receiveUpdatesUnderlineNo.setVisibility(View.VISIBLE);
                receiveUpdatesUnderlineYes.setVisibility(View.INVISIBLE);
                receiveUpdates = false;
                break;

            default:
                break;
        }
    }

    private boolean isInputsValid(String companyName, String username, String email, String password,
                                  String phoneNumber) {
        boolean isValid = false;
        if (!ValidationUtil.isUsernameValid(username)) {
            providerNameEdt.setError("Name can't be empty");
        } else if (!ValidationUtil.isUsernameValid(companyName)) {
            companyNameEdt.setError("Company name can't be empty");
        } else if (!ValidationUtil.isEmailValid(email)) {
            providerEmailEdt.setError("Invalid Email");
        } else if (!ValidationUtil.isPasswordValid(password)) {
            providerPasswordEdt.setError("Password should be longer than six");
        } else if (!ValidationUtil.isMobilePhoneValid(phoneNumber)) {
            providerPhoneEdt.setError("Invalid phone number");
        } else {
            isValid = true;
        }
        return isValid;
    }

    private String getCountryCode() {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryID = manager.getSimCountryIso().toUpperCase();
        String[] countryCodesFromResources = this.getResources().getStringArray(R.array.country_codes);
        String countryCode = "";
        for (String resCountryCode : countryCodesFromResources) {
            String[] g = resCountryCode.split(",");
            if (g[0].trim().equals(countryID.trim())) {
                Log.e(TAG, "Country code" + g[1]);
                countryCode = g[1];
                break;
            }
        }
        return countryCode;
    }

    private String getRegistrationId() {
        String registrationId = PrefUtil.getDeviceID(context);
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = PrefUtil.getAppVersion(context);
        int currentVersion = getAppVersion(context);
        PrefUtil.setAppVersion(context, currentVersion);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void registerInBackground() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    PrefUtil.setDeviceID(context, gcm.register(GCM_SENDER_ID));
                    Log.i(TAG, "Device registered, registration ID=" + PrefUtil.getDeviceID(context));
                } catch (IOException ex) {
                    Log.e(TAG, ex.getLocalizedMessage(), ex);
                }
                return msg;
            }

            @SuppressWarnings("unused")
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }

    private int getIndexOfWhichEndWith(String countryCode) {
        List<String> countryCodesArray = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.country_codes)));
        for (String code : countryCodesArray) {
            if (code.endsWith(countryCode)) {
                return countryCodesArray.indexOf(code);
            }
        }
        return -1;
    }

    public class ResponseListener implements DialogListener {
        @Override
        public void onBack() {
        }

        @Override
        public void onCancel() {
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onComplete(Bundle arg0) {
            token = socialAuthAdapter.getCurrentProvider().getAccessGrant().getKey();
            socialAuthAdapter.getUserProfileAsync(new ProfileDataListener());
        }

        @Override
        public void onError(SocialAuthError arg0) {
        }
    }

    @SuppressWarnings("rawtypes")
    class ProfileDataListener implements SocialAuthListener {
        @Override
        public void onError(SocialAuthError arg0) {
        }

        @SuppressWarnings("static-access")
        @Override
        public void onExecute(String arg0, Object arg1) {
            Log.d("Custom-UI", "Receiving Data");

            profileMap = socialAuthAdapter.getUserProfile();

            String loginType = PrefUtil.getSocialLoginType(context);

            if (loginType.equalsIgnoreCase("googleplus")) {
                PrefUtil.setGoogleId(context, profileMap.getValidatedId());
                PrefUtil.setGoogleEmail(context, profileMap.getEmail());
                PrefUtil.setGoogleName(context, profileMap.getFirstName());
                socialAuthAdapter.signOut(context, SocialAuthAdapter.Provider.GOOGLEPLUS.toString());

            } else if (loginType.equalsIgnoreCase("facebook")) {
                PrefUtil.setFacebookId(context, profileMap.getValidatedId());
                PrefUtil.setFacebookEmail(context, profileMap.getEmail());
                PrefUtil.setFacebookName(context, profileMap.getFirstName());
                socialAuthAdapter.signOut(context, SocialAuthAdapter.Provider.FACEBOOK.toString());
            }

            getFragmentManager().beginTransaction()
                    .replace(R.id.sign_up_act_content_frame, new SignUpVerifyPhoneFragment())
                    .commit();
        }
    }

}
