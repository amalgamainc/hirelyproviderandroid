package com.hirely.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.hirelyprovider.app.R;

public class SupportFragment extends Fragment implements OnClickListener {

    Button sendFeedbackBtn;
    Button faqBtn;
    EditText feedbackEdt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_support, container, false);

        ActionBar ab = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (ab != null) {
            ab.setTitle("SUPPORT");
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sendFeedbackBtn = (Button) view.findViewById(R.id.support_fr_send_feedback_btn);
        sendFeedbackBtn.setOnClickListener(this);
        faqBtn = (Button) view.findViewById(R.id.support_fr_faq_btn);
        faqBtn.setOnClickListener(this);
        feedbackEdt = (EditText) view.findViewById(R.id.support_fr_comment_edt);
    }

    @Override
    public void onClick(View v) {
        final int veiwId = v.getId();

        switch (veiwId) {
            case R.id.support_fr_send_feedback_btn:
                sendFeedBack(String.valueOf(feedbackEdt.getText()));
                break;
            case R.id.support_fr_faq_btn:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.sparadic.com/"));
                startActivity(browserIntent);
                break;
        }
    }

    private void sendFeedBack(String massage) {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.setType("text/email");
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@sparadic.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
        email.putExtra(Intent.EXTRA_TEXT, massage);
        startActivity(Intent.createChooser(email, "Send Feedback:"));
    }
}