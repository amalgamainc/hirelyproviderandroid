package com.hirely.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.GlobalConstants;
import com.hirely.activity.MainActivity;
import com.hirely.entity.Job;
import com.hirely.service.GPSTrackerService;
import com.hirely.util.PrefUtil;
import com.hirely.util.WebServiceHandler;
import com.hirelyprovider.app.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SuppressLint({"HandlerLeak", "NewApi"})
public class NewJobFragment extends Fragment {

    private static final String TAG = NewJobFragment.class.getSimpleName();


    private ListView jobListView;
    private ProgressDialog progressDialog, pd1;
    private Dialog dialog;
    private List<Job> jobList = new ArrayList<>();
    private BaseAdapter adapter;


    class LoadJobListTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd1 = ProgressDialog.show(getActivity(), "", "Loading New jobs...");

        }

        @Override
        protected Void doInBackground(Void... voids) {
            jobList = WebServiceHandler.getRequestedJobList(getActivity());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd1.dismiss();
            if (jobList.size() > 0) {
                adapter = new JobsAdapter(getActivity(), jobList);
                jobListView.setAdapter(adapter);
            } else {
                Toast.makeText(getActivity(), "No requested jobs", Toast.LENGTH_LONG).show();
            }
        }
    }


    class AcceptJobTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), "", "Accepting job...");
        }

        @Override
        protected String doInBackground(String... data) {

            String res = WebServiceHandler.acceptJob(getActivity(), data[0]);
            if (res.equalsIgnoreCase("JOB_ACCEPTED")) {
                return WebServiceHandler.updateProviderStatus(getActivity(), GlobalConstants.PROVIDER_STATUS_1_CLIENT_SELECTED);
            } else if (res.equalsIgnoreCase("JOB_ALREADY_CANCELED")) {
                return res;
            }
            return "0";
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            progressDialog.dismiss();
            if (res.equalsIgnoreCase("JOB_ALREADY_CANCELED")) {
                Toast.makeText(getActivity(), "Job doesn't exist", Toast.LENGTH_LONG).show();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new NewJobFragment())
                        .commit();
            } else if (res.equals("1")) {
                PrefUtil.setProviderStatus(getActivity(), GlobalConstants.PROVIDER_STATUS_1_CLIENT_SELECTED);
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new ProviderInTheWayFragment())
                        .commit();
            } else {
                Toast.makeText(getActivity(), "Can't update provider status", Toast.LENGTH_LONG).show();
            }
        }
    }

    class DeclineJobTask extends AsyncTask<Void, Void, String> {

        private int position;

        public DeclineJobTask(int position) {
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(getActivity(), "", "Declining job...");
        }

        @Override
        protected String doInBackground(Void... params) {
            return WebServiceHandler.declineJob(getActivity());
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equalsIgnoreCase(WebServiceHandler.SUCCESS)) {
                jobList.remove(position);
                adapter.notifyDataSetChanged();
            } else {
                Toast.makeText(getActivity(), "Some error occured..Please Try again..", Toast.LENGTH_LONG).show();
            }
            progressDialog.dismiss();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_job, container, false);

        GPSTrackerService.updateLocationInPreferences(getActivity());
        jobListView = (ListView) view.findViewById(R.id.joblist_listview);

        new LoadJobListTask().execute();

        jobListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Job selectedJob = jobList.get(position);
                Log.i(TAG, "Selected job: " + selectedJob);

                PrefUtil.setJobUserId(getActivity(), selectedJob.userId);
                PrefUtil.setJobAddress(getActivity(), selectedJob.address);
                PrefUtil.setJobLongitude(getActivity(), selectedJob.longitude);
                PrefUtil.setJobLatitude(getActivity(), selectedJob.latitude);
                PrefUtil.setJobServiceId(getActivity(), selectedJob.serviceId);

                displayAcceptOrDeclineJobDialog(position);
            }
        });

        return view;
    }

    private void displayAcceptOrDeclineJobDialog(final int position) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_accept_job);
        dialog.show();

        Button acceptBtn = (Button) dialog.findViewById(R.id.accept);
        Button declineBtn = (Button) dialog.findViewById(R.id.decline);

        acceptBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                PrefUtil.setServiceList(getActivity(), jobList.get(position).getRequestedServicesAsString("\n"));
                PrefUtil.setJobCost(getActivity(), String.valueOf(((MainActivity) getActivity()).providerInformation.serviceTotalCost( jobList.get(position).requestedServices )));
                new AcceptJobTask().execute(jobList.get(position).getRequestedServicesAsString("\n"));

            }
        });
        declineBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                PrefUtil.setServiceList(getActivity(), "");
                PrefUtil.setJobCost(getActivity(), "");
                new DeclineJobTask(position).execute();
            }
        });
    }

    class JobsAdapter extends BaseAdapter {

        public List<Job> jobList;
        TextView job_txt;
        private Activity context;

        public JobsAdapter(Activity context, List<Job> jobList) {
            super();
            this.context = context;
            this.jobList = jobList;
            Log.e(TAG, "Job list: " + jobList);
        }

        @Override
        public int getCount() {
            return jobList.size();
        }

        @Override
        public Object getItem(int position) {
            return jobList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rootview = inflater.inflate(R.layout.job_list_item, null, true);

            float distanceToClient = Float.parseFloat(jobList.get(position).distance);
            DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols(Locale.getDefault());
            unusualSymbols.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("#.##", unusualSymbols);
            String dx = df.format(distanceToClient);
            distanceToClient = Float.valueOf(dx);

            StringBuilder jobDescription = new StringBuilder(100);

            jobDescription.append(jobList.get(position).userName);
            jobDescription.append("\n");
            jobDescription.append(jobList.get(position).address);
            jobDescription.append("\n");
            jobDescription.append(distanceToClient);
            jobDescription.append(" km\nServices:\n");
            jobDescription.append(jobList.get(position).getRequestedServicesAsString("\n").toUpperCase());

            job_txt = (TextView) rootview.findViewById(R.id.job_txt_first);
            job_txt.setText(jobDescription.toString().trim());

            return rootview;
        }
    }
}