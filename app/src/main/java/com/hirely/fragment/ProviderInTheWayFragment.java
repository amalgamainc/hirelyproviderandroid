package com.hirely.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hirely.GlobalConstants;
import com.hirely.service.GPSTrackerService;
import com.hirely.util.PrefUtil;
import com.hirely.util.WebServiceHandler;
import com.hirelyprovider.app.R;

@SuppressLint("NewApi")
public class ProviderInTheWayFragment extends Fragment implements OnMapReadyCallback {
    public static final double JOB_START_RADIUS = 0.3;
    private static final String TAG = ProviderInTheWayFragment.class.getSimpleName();

    private static View rootView;
    private Button beginServiceBtn;
    private TextView clientAddressTxt;
    private TextView username;
    private RelativeLayout header;

    private String dialNumber;
    private MyLocationListener locationListener;
    private Criteria criteria;
    private String provider;
    private LocationManager locationManager;
    private Marker providerMarker;
    private Dialog dialog;


    class GetServiceInfoTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.serviceDetails(getActivity());
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res.equalsIgnoreCase(WebServiceHandler.SUCCESS)) {
                updateDistance();
                allowStartWorkIfInRadius();
            } else {
                Toast.makeText(getActivity(), "" + PrefUtil.getRegisterMessage(getActivity()), Toast.LENGTH_LONG).show();
            }
        }
    }


    class ProviderInTheWayTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.updateProviderStatus(getActivity(), GlobalConstants.PROVIDER_STATUS_2_IN_THE_WAY_TO_CLIENT);
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res.equals("1")) {
                beginServiceBtn.setVisibility(View.VISIBLE);
                if (isAdded()) {
                    PrefUtil.setProviderStatus(getActivity(), GlobalConstants.PROVIDER_STATUS_2_IN_THE_WAY_TO_CLIENT);
                }
            }
        }
    }

    class GetClientInfoTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.getUserInfo(getActivity());
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res.equalsIgnoreCase(WebServiceHandler.SUCCESS)){
                if(isAdded()) {
                    username.setText(PrefUtil.getJobUserUsername(getActivity()));
                    dialNumber = PrefUtil.getJobPhone(getActivity());
                }
            }
        }
    }

    class StartJobTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            String res = WebServiceHandler.startJob(getActivity());
            if (res.equalsIgnoreCase(WebServiceHandler.SUCCESS)){
                res = WebServiceHandler.updateProviderStatus(getActivity(), GlobalConstants.PROVIDER_STATUS_3_JOB_IN_PROGRESS);
            }
            return res;
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            if (res.equals("1")) {
                PrefUtil.setProviderStatus(getActivity(), GlobalConstants.PROVIDER_STATUS_3_JOB_IN_PROGRESS);
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new ServicInProcessFragment())
                        .commit();
            } else {
                Toast.makeText(getActivity(), "Can't start job", Toast.LENGTH_LONG).show();
            }
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            if (rootView == null) {
                rootView = inflater.inflate(R.layout.fragment_provider_in_the_way, container, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        GPSTrackerService.updateLocationInPreferences(getActivity());

        init();

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (status == ConnectionResult.SUCCESS) {
            new GetServiceInfoTask().execute();
        } else if (status == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
            hideControls();
        }

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the location provider
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);    //default
        provider = locationManager.getBestProvider(criteria, false);
        locationListener = new MyLocationListener();
        locationManager.requestLocationUpdates(provider, 200, 1, locationListener);

        return rootView;
    }

    private void hideControls() {
        header.setVisibility(View.GONE);
        beginServiceBtn.setVisibility(View.GONE);
    }

    private void updateDistance() {
        if (isAdded()) {
            String address = PrefUtil.getJobAddress(getActivity()) + "\n" + calculateDistance() + "Km";
            clientAddressTxt.setText(address);
        }
    }

    private double calculateDistance() {
        return Math.round(Float.parseFloat(PrefUtil.getJobDistance(getActivity())) * 1000.0) / 1000.0;
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeMap();
    }

    private void initializeMap() {
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.map_container_begin, mapFragment).commit();
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Fragment fragment = getFragmentManager().findFragmentById(R.id.map_container_begin);
        if (fragment != null) {
            getFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onPause();
    }

    private void showNumberErrorDialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_mob_number_error);
        dialog.show();
        dialog.findViewById(R.id.ok_btn)
                .setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void init() {
        beginServiceBtn = (Button) rootView.findViewById(R.id.prov_way_fr_begin_service_btn);
        beginServiceBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //new Thread(startJob).start();
                Toast.makeText(getActivity(), "Press and hold to begin", Toast.LENGTH_SHORT).show();
            }
        });
        beginServiceBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                new StartJobTask().execute();
                return false;
            }
        });
        beginServiceBtn.setVisibility(PrefUtil.getProviderStatus(getActivity()).equalsIgnoreCase(GlobalConstants.PROVIDER_STATUS_2_IN_THE_WAY_TO_CLIENT) ? View.VISIBLE : View.GONE);

        username = (TextView) rootView.findViewById(R.id.username);

        //new Thread(getUserInfo).start();
        new GetClientInfoTask().execute();

        clientAddressTxt = (TextView) rootView.findViewById(R.id.prov_way_fr_client_address);
        header = (RelativeLayout) rootView.findViewById(R.id.prov_way_fr_header);

        ImageView callToClientBtn = (ImageView) rootView.findViewById(R.id.prov_way_fr_call_to_client);
        callToClientBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialNumber != null && dialNumber.length() == 0) {
                    showNumberErrorDialog();
                } else {
                    try {
                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + dialNumber)));
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "You don't have app to call", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        ImageView smsToClientBtn = (ImageView) rootView.findViewById(R.id.prov_way_fr_sms_to_client);
        smsToClientBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialNumber.length() == 0) {
                    showNumberErrorDialog();
                } else {
                    try {
                        Intent sms = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", dialNumber, null));
                        startActivity(sms);
                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "You don't have app to send SMS", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        GPSTrackerService gps = new GPSTrackerService(getActivity());
        // add provider marker to map
        providerMarker = googleMap.addMarker(new MarkerOptions()
                .title("You're here")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.spa_location))
                .position(new LatLng(gps.getLatitude(), gps.getLongitude())));

        Log.e(TAG, "Location in prefs: " + PrefUtil.getJobLatitude(getActivity()) + "  : " +
                PrefUtil.getJobLongitude(getActivity()));
        // add user marker to map
        Log.i(TAG, "User coordinates: " + PrefUtil.getJobLatitude(getActivity()) + " " + PrefUtil.getJobLongitude(getActivity()));
        googleMap.addMarker(new MarkerOptions()
                .title("User")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location))
                .position(new LatLng(
                        Double.parseDouble(PrefUtil.getJobLatitude(getActivity())),
                        Double.parseDouble(PrefUtil.getJobLongitude(getActivity())))));

        googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(new LatLng(gps.getLatitude(), gps.getLongitude()), 12));

        googleMap.setOnMapClickListener(new OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                Toast.makeText(getActivity(), "You're here" + point.latitude + " " + point.longitude,
                        Toast.LENGTH_LONG).show();
            }
        });

        gps.stopUsingGPS();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(locationListener);
    }

    private void allowStartWorkIfInRadius() {
        if (isAdded()) {
            if (calculateDistance() < JOB_START_RADIUS) {
                new ProviderInTheWayTask().execute();
            }
        }
    }

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(final Location location) {

            new AsyncTask<Void, Void, Boolean>() {

                @Override
                protected Boolean doInBackground(Void... params) {
                    Log.i(TAG, "New location was sent...");
                    return WebServiceHandler.updateProviderLocation(getActivity(), location);
                }

                @Override
                protected void onPostExecute(Boolean result) {
                    super.onPostExecute(result);
                    if (result) {
                        updateDistance();
                    }
                }
            }.execute();

            if (providerMarker != null) {
                providerMarker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
            }

            allowStartWorkIfInRadius();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Toast.makeText(getActivity(), provider + "'s status changed to " + status + "!",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(getActivity(), "Provider " + provider + " enabled!",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getActivity(), "Provider " + provider + " disabled!",
                    Toast.LENGTH_SHORT).show();
        }
    }
}