package com.hirely.fragment;

import android.annotation.SuppressLint;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.activity.MainActivity;
import com.hirely.entity.Provider;
import com.hirely.util.PrefUtil;
import com.hirely.util.ValidationUtil;
import com.hirely.util.WebServiceHandler;
import com.hirelyprovider.app.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressLint("NewApi")
public class ProfileFragment extends Fragment {

    private static final String TAG = ProfileFragment.class.getSimpleName();

    private EditText providerNameEdt, providerEmailEdt, providerMobileEdt;
    private EditText companyNameEdt;
    private Spinner countryCodeSpnr;
    private TextView saveProfileBtn;
    private TextView titleUserName;
    private TextView totalEarnTxt;

    private String oldFullMobileNumber;
    private String companyName;
    private String username;
    private String mobileNumber;
    private String email;
    private String countryCode;

    class GetProviderInfoTask extends AsyncTask<Void, Void, Provider> {
        @Override
        protected Provider doInBackground(Void... voids) {
            WebServiceHandler.getProviderTotalEarn(getActivity());
            return WebServiceHandler.providerInformation(getActivity());
        }
        @Override
        protected void onPostExecute(Provider s) {
            super.onPostExecute(s);
            if (s != null) {
                if(isAdded()) {
                    getUserProfile();
                }
            } else {
                Toast.makeText(getActivity(), "Can't take provider info", Toast.LENGTH_LONG).show();
            }
        }
    }

    class EditProfileTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.editProviderProfile(getActivity(), companyName, username, email, mobileNumber, countryCode);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equalsIgnoreCase(WebServiceHandler.SUCCESS)) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                Toast.makeText(getActivity(), "Changes applied", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Can't edit profile", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        companyNameEdt = (EditText) view.findViewById(R.id.profile_fr_company_name_edt);
        providerNameEdt = (EditText) view.findViewById(R.id.profile_fr_provider_name_edt);
        providerEmailEdt = (EditText) view.findViewById(R.id.profile_fr_email_edt);
        providerMobileEdt = (EditText) view.findViewById(R.id.profile_fr_mobile_edt);
        titleUserName = (TextView) view.findViewById(R.id.profile_fr_user_name_title);
        totalEarnTxt = (TextView) view.findViewById(R.id.profile_fr_total_earn);
        countryCodeSpnr = (Spinner) view.findViewById(R.id.profile_fr_country_code_spnr);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.country_codes, R.layout.custom_spinner_item);
        adapter.setDropDownViewResource(R.layout.custom_spinner_item);
        countryCodeSpnr.setAdapter(adapter);
        saveProfileBtn = (TextView) view.findViewById(R.id.profile_fr_save_btn);
        saveProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                companyName = companyNameEdt.getText().toString();
                username = providerNameEdt.getText().toString();
                mobileNumber = providerMobileEdt.getText().toString();
                email = providerEmailEdt.getText().toString();
                countryCode = countryCodeSpnr.getSelectedItem().toString().split(", ")[1];
                String fullMobileNumber = countryCode + mobileNumber;

                if (isInputsValid(companyName, username, email, mobileNumber)) {
                    if (fullMobileNumber.equals(oldFullMobileNumber)) {
                        new EditProfileTask().execute();
                    } else {
                        Log.i(TAG, "Going to verify...");
                        SignUpVerifyPhoneFragment fragment = new SignUpVerifyPhoneFragment();
                        fragment.setArguments(packDataInBundle(companyName, username, email, mobileNumber, countryCode));
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
                    }
                }
            }
        });

        getUserProfile();
        new GetProviderInfoTask().execute();
    }

    private void getUserProfile() {
        String companyName = PrefUtil.getCompanyName(getActivity());
        String email = PrefUtil.getProviderEmail(getActivity());
        String oldMobileNumber = PrefUtil.getProviderPhone(getActivity());
        String username = PrefUtil.getProviderName(getActivity());
        String oldCountryCode = PrefUtil.getProviderPhoneCountryCode(getActivity());
        String totalEarn = PrefUtil.getEarnedTotal(getActivity());
        Log.i(TAG, "Data: " + email + " " + mobileNumber);
        oldFullMobileNumber = oldCountryCode + oldMobileNumber;

        companyNameEdt.setText(companyName);
        providerEmailEdt.setText(email);
        providerMobileEdt.setText(oldMobileNumber);
        providerNameEdt.setText(username);
        titleUserName.setText(username);
        countryCodeSpnr.setSelection(getIndexOfWhichEndWith(oldCountryCode));
        totalEarnTxt.setText("Total Earnings: " + totalEarn + " PHP");
    }

    private int getIndexOfWhichEndWith(String countryCode) {
        List<String> countryCodesArray = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.country_codes)));
        for (String code : countryCodesArray) {
            if (code.endsWith(countryCode)) {
                return countryCodesArray.indexOf(code);
            }
        }
        return -1;
    }

    private boolean isInputsValid(String companyName, String username, String email, String mobileNumber) {
        boolean isValid = false;
        if (!ValidationUtil.isUsernameValid(username)) {
            providerNameEdt.setError("Name can't be empty");
        } else if (!ValidationUtil.isUsernameValid(companyName)) {
            companyNameEdt.setError("Company name can't be empty");
        } else if (!ValidationUtil.isEmailValid(email)) {
            providerEmailEdt.setError("Invalid Email");
        } else if (!ValidationUtil.isMobilePhoneValid(mobileNumber)) {
            providerMobileEdt.setError("Invalid phone number");
        } else {
            isValid = true;
        }
        return isValid;
    }

    private Bundle packDataInBundle(String companyName, String username, String email, String mobileNumber, String countryCode) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isUpdate", true);
        bundle.putString("username", username);
        bundle.putString("email", email);
        bundle.putString("phoneNumber", mobileNumber);
        bundle.putString("countryCode", countryCode);
        bundle.putString("companyName", companyName);
        return bundle;
    }
}