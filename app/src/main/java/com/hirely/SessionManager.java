package com.hirely;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.hirely.activity.LogInActivity;
import com.hirely.util.PrefUtil;

public class SessionManager {

    private SessionManager() {
    }

    private static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void createLoginSession(Context c, String name, String pass) {
        getPrefs(c).edit().putString("rememberEmail", name).commit();
        getPrefs(c).edit().putString("rememberPassword", pass).commit();
        getPrefs(c).edit().putBoolean("isLoggedIn", true).commit();
    }

    public static void logout(Context context) {
        getPrefs(context).edit().putBoolean("isLoggedIn", false).commit();
        getPrefs(context).edit().putString("rememberEmail", null).commit();
        getPrefs(context).edit().putString("rememberPassword", null).commit();
        PrefUtil.setGoogleStatus(context, false);
        PrefUtil.setFacebookStatus(context, false);




        Intent intent = new Intent(context, LogInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    public static boolean isLoggedIn(Context c) {
        return getPrefs(c).getBoolean("isLoggedIn", false);
    }
}