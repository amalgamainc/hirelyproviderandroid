package com.hirely.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.GlobalConstants;
import com.hirely.SessionManager;
import com.hirely.entity.Job;
import com.hirely.entity.Provider;
import com.hirely.fragment.ServicInProcessFragment;
import com.hirely.fragment.FeedbackFragment;
import com.hirely.fragment.NewJobFragment;
import com.hirely.fragment.ProfileFragment;
import com.hirely.fragment.ProviderInTheWayFragment;
import com.hirely.fragment.SupportFragment;
import com.hirely.service.GcmIntentService;
import com.hirely.util.PrefUtil;
import com.hirely.util.WebServiceHandler;
import com.hirelyprovider.app.R;

import java.io.Serializable;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

@SuppressLint("NewApi")
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String PROVIDER_INFO_KEY = "providerInformation";
    private static final String CURRENT_JOB_KEY = "currentJob";

    private RelativeLayout drawerMenuNewJobs;
    private RelativeLayout grawerMenuProfile;
    private RelativeLayout drawerMenuSupport;
    private RelativeLayout drawerMenuLogout;
    private Dialog dialog;
    private Fragment fragment;
    private TextView drawerProviderName;
    private ProgressDialog progressDialog;
    private DrawerLayout drawerLayout;
    private RelativeLayout drawerView;
    private Toolbar toolbar;

    public Provider providerInformation;

    class UpdateProviderStatusTask extends AsyncTask<Void, Void, Provider> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MainActivity.this, "", "Loading...");
        }
        @Override
        protected Provider doInBackground(Void... voids) {
            return WebServiceHandler.providerInformation(MainActivity.this);
        }
        @Override
        protected void onPostExecute(Provider res) {
            super.onPostExecute(res);
            progressDialog.dismiss();
            if (res != null) {

                drawerProviderName.setText(PrefUtil.getProviderName(MainActivity.this));
                switchFragmentBasedOnStatus();
                drawerLayout.closeDrawer(drawerView);

                providerInformation = res;

            } else {
                Toast.makeText(getApplicationContext(), "Invalid entries", Toast.LENGTH_LONG).show();
            }
        }
    }

    class UpdateProviderOnlineStatus extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return WebServiceHandler.updateProviderOnLineStatus(MainActivity.this, params[0]);
        }
    }



    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        public void onReceive(final Context context, Intent intent) {

            String massage = intent.getExtras().getString("type");
            if (massage != null) {
                switch (massage) {
                    case "NEW_JOB":
                        displayNewJobDialog();
                        break;
                    case "JOB_CANCELED":

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                WebServiceHandler.updateProviderStatus(context, GlobalConstants.PROVIDER_STATUS_0_WAIT_FOR_NEW_JOB);
                                PrefUtil.setProviderStatus(context, GlobalConstants.PROVIDER_STATUS_0_WAIT_FOR_NEW_JOB);
                            }
                        }).start();

                        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new NewJobFragment()).commitAllowingStateLoss();
                        displayJobCanceledMwssage();
                        break;
                }
            }
        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initMenu();
        initToolBar();

        int lastNotificationID = PrefUtil.getLastNotificationID(this);
        if (lastNotificationID > 0) {
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.cancel(lastNotificationID);
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(
                messageReceiver, new IntentFilter(GcmIntentService.BROADCAST_ACTION));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerView = (RelativeLayout) findViewById(R.id.left_drawer);

        new UpdateProviderStatusTask().execute();
        new UpdateProviderOnlineStatus().execute(GlobalConstants.PROVIDER_ONLINE_STATUS_1_ONLINE);



    }

    private void restoreServicePrices (Bundle savedInstanceState) {
        Serializable s = savedInstanceState.getSerializable(PROVIDER_INFO_KEY);
        if (s instanceof Provider) {
            providerInformation = (Provider) s;
        }
        if (providerInformation == null || providerInformation.isServicePricesEmpty()) {
            new UpdateProviderStatusTask().execute();
        }
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PROVIDER_INFO_KEY, providerInformation);


    }

    private void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.green));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setIcon(android.R.color.transparent);
            getSupportActionBar().setTitle("SPARADIC");
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (drawerLayout.isDrawerOpen(drawerView)) {
                drawerLayout.closeDrawer(drawerView);
            } else {
                drawerLayout.openDrawer(drawerView);
            }
        }
        return true;
    }

    private void initMenu() {
        drawerMenuNewJobs = (RelativeLayout) findViewById(R.id.drawer_new_job);
        grawerMenuProfile = (RelativeLayout) findViewById(R.id.drawer_profile);
        drawerMenuSupport = (RelativeLayout) findViewById(R.id.drawer_support);
        drawerMenuLogout = (RelativeLayout) findViewById(R.id.drawer_logout);
        drawerProviderName = (TextView) findViewById(R.id.drawer_provider_name);
        drawerMenuNewJobs.setOnClickListener(this);
        grawerMenuProfile.setOnClickListener(this);
        drawerMenuSupport.setOnClickListener(this);
        drawerMenuLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        fragment = null;
        Bundle bundle = new Bundle();
        if (v.getId() == R.id.drawer_new_job) {
            switchFragmentBasedOnStatus();
        } else if (v.getId() == R.id.drawer_profile) {
            fragment = new ProfileFragment();
            switchFragment(fragment);
        } else if (v.getId() == R.id.drawer_support) {
            fragment = new SupportFragment();
            switchFragment(fragment);
        } else if (v.getId() == R.id.drawer_logout) {
            String socialLoginType = PrefUtil.getSocialLoginType(this);
            if (socialLoginType != null) {
                if (socialLoginType.equalsIgnoreCase("facebook")) {
                    PrefUtil.setFacebookStatus(this, false);
                    startActivity(new Intent(MainActivity.this, LogInActivity.class));
                } else if (socialLoginType.equalsIgnoreCase("googleplus")) {
                    PrefUtil.setGoogleStatus(this, false);
                    startActivity(new Intent(MainActivity.this, LogInActivity.class));
                }
            } else {
                SessionManager.logout(this);
            }
            new UpdateProviderOnlineStatus().execute(GlobalConstants.PROVIDER_ONLINE_STATUS_0_OFFLINE);
            finish();
        }

        if (fragment != null) {
            fragment.setArguments(bundle);
            switchFragment(fragment);
        }
    }

    private void switchFragmentBasedOnStatus() {
        String status = PrefUtil.getProviderStatus(this);
        switch (status) {
            case GlobalConstants.PROVIDER_STATUS_0_WAIT_FOR_NEW_JOB:
                switchFragment(new NewJobFragment());
                break;
            case GlobalConstants.PROVIDER_STATUS_1_CLIENT_SELECTED:
            case GlobalConstants.PROVIDER_STATUS_2_IN_THE_WAY_TO_CLIENT:
                switchFragment(new ProviderInTheWayFragment());
                break;
            case GlobalConstants.PROVIDER_STATUS_3_JOB_IN_PROGRESS:
                switchFragment(new ServicInProcessFragment());
                break;
            case GlobalConstants.PROVIDER_STATUS_4_FEEDBACK:
                switchFragment(new FeedbackFragment());
                break;
            default:
                switchFragment(new NewJobFragment());
                break;
        }
    }

    private void switchFragment(Fragment fragment) {
        drawerLayout.closeDrawer(drawerView);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commitAllowingStateLoss();
    }

    @Override
    protected void onResume() {
        PrefUtil.setAppInBackground(this, false);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
    }

    @Override
    protected void onPause() {
        PrefUtil.setAppInBackground(this, true);
        super.onPause();
    }

    private void displayJobCanceledMwssage() {
        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_new_job);
        dialog.show();

        TextView title = (TextView) dialog.findViewById(R.id.text1);
        title.setText("Message");

        TextView text = (TextView) dialog.findViewById(R.id.lorem);
        text.setText("Job was canceled");

        Button okBtn = (Button) dialog.findViewById(R.id.see_btn);
        okBtn.setText("OK");

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void displayNewJobDialog() {
        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_new_job);
        dialog.show();

        Button seeBtn = (Button) dialog.findViewById(R.id.see_btn);

        seeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new NewJobFragment()).commit();
            }
        });
    }
}