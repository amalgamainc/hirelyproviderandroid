package com.hirely.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.hirely.SessionManager;
import com.hirely.fragment.AttentionFragment;
import com.hirely.util.CheckAppAvailability;
import com.hirely.util.PrefUtil;
import com.hirelyprovider.app.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CheckAppAvailability.isInternetOn(this)) {
            if (CheckAppAvailability.isGPSOn(this)) {
                startWorkWithApp();
            } else {
                CheckAppAvailability.showMessage(this, CheckAppAvailability.Type.GPS);
            }
        } else {
            CheckAppAvailability.showMessage(this, CheckAppAvailability.Type.INTERNET);
        }
    }

    private void startWorkWithApp() {
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SessionManager.isLoggedIn(SplashActivity.this)) {
                    if (PrefUtil.getProviderVerification(SplashActivity.this).equals("1")) {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        finish();
                    } else if (PrefUtil.getProviderVerification(SplashActivity.this).equals("0")) {
                        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new AttentionFragment()).commit();
                        findViewById(R.id.logo).setVisibility(View.GONE);
                    }
                } else {
                    startActivity(new Intent(SplashActivity.this, LogInActivity.class));
                    finish();
                }

            }
        }, 3000);
    }
}
