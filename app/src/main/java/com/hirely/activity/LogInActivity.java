package com.hirely.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.hirely.fragment.LogInFragment;
import com.hirely.util.PrefUtil;
import com.hirelyprovider.app.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LogInActivity extends AppCompatActivity {


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PrefUtil.setSocialLoginType(this, null);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_log_in);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new LogInFragment()).commit();
    }
}