package com.hirely.activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hirely.util.ValidationUtil;
import com.hirely.util.WebServiceHandler;
import com.hirelyprovider.app.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgetPasswordActivity extends AppCompatActivity {

    EditText emailEdt, passwordEdt;
    Button submitBtn;
    String emailStr, passStr;
    ProgressDialog progressDialog;

    class ChangePasswordTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ForgetPasswordActivity.this, "Please wait...", "Loading...");
        }

        @Override
        protected String doInBackground(Void... voids) {
            return WebServiceHandler.forgetPassword(ForgetPasswordActivity.this, emailStr, passStr);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (s.equalsIgnoreCase(WebServiceHandler.SUCCESS)) {
                startActivity(new Intent(ForgetPasswordActivity.this, MainActivity.class));
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Error occurred", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_forget_password);

        init();
    }

    private void init() {
        emailEdt = (EditText) findViewById(R.id.forget_pass_act_email);
        passwordEdt = (EditText) findViewById(R.id.forget_pass_act_password);
        submitBtn = (Button) findViewById(R.id.forget_pass_act_password_submit_btn);
        submitBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                emailStr = emailEdt.getText().toString().trim();
                passStr = passwordEdt.getText().toString().trim();
                validateData(emailStr, passStr);
            }
        });
    }

    private void validateData(String email, String password) {
        if (ValidationUtil.isEmailValid(email)) {
            Toast.makeText(ForgetPasswordActivity.this, "Email isn't valid.", Toast.LENGTH_SHORT).show();
        } else if (ValidationUtil.isPasswordValid(password)) {
            Toast.makeText(ForgetPasswordActivity.this, "Password is too short.", Toast.LENGTH_SHORT).show();
        } else {
            new ChangePasswordTask().execute();
        }
    }
}