package com.hirely;

public class GlobalConstants {

    public static String url = "http://sparadicph.com/index.php/Webservices";

    public static String PREF_NAME = "com.hirelyprovider.app";
    // =======================SIGN UP ======================
    public static String USERNAME = "username";
    public static String EMAIL = "email";
    public static String PASSWORD = "password";
    public static String VERIFY_MOB = "mobileno";
    public static String DEVICEID = "device_id";
    public static String DEVICETYPE = "device_type";
    public static String LATITUDE = "lat";
    public static String LONGITUDE = "lon";
    public static String SERVICETYPE = "service_type";
    public static String STATUS = "status";
    public static String CODE = "code";

    // =======================SIGN IN ======================
    public static String loginprovider = "username";
    public static String passwordprovider = "password";
    public static String USERTYPE = "type";
    public static String PROVIDERSTATUS = "provider_status";

    // =======================SOCIAL SIGN UP ======================
    public static String SOCIALUSERNAME = "username";
    public static String SOCIALEMAIL = "email";
    public static String SOCIALUNIQUEID = "unique_id";

    // =======================SERVICE INFO ======================
    public static String SERVICEID = "id";
    // =======================PROVIDER INFO ======================
    public static String PROVIDERSERVICETYPE = "service_type";
    public static String PROVIDER_ID = "id";

    // =======================PROVIDER NOTIFICATION ======================
    public static String pNotificationUserID = "user_id";
    public static String pNotificationId = "id";
    public static String pNotificationLat = "lat";
    public static String pNotificationLon = "lon";
    public static String pNotificationServiceType = "service_type";
    public static String pNotificationStatus = "status";
    // ==================TIMER ======================================
    public static String TIME = "time";
    public static String COST = "cost";
    // =================FORGET PASSWORD===========================
    public static String FORGET_EMAIL = "email";
    public static String FORGET_PASS = "password";
    // ==================STATUS UPDATE=========================
    public static String PROSTATUS = "status";
    public static String PROID = "id";
    //FEEDBACK
    public static String SERVICE_ID = "service_id";
    public static String COMMENT = "comments";
    public static String RATING = "rating";
    public static String IS_PROVIDER = "is_provider";


    public static final String PROVIDER_STATUS_0_WAIT_FOR_NEW_JOB = "0";
    public static final String PROVIDER_STATUS_1_CLIENT_SELECTED = "1";
    public static final String PROVIDER_STATUS_2_IN_THE_WAY_TO_CLIENT = "2";
    public static final String PROVIDER_STATUS_3_JOB_IN_PROGRESS = "3";
    public static final String PROVIDER_STATUS_4_FEEDBACK = "4";

    public static final String PROVIDER_ONLINE_STATUS_0_OFFLINE = "0";
    public static final String PROVIDER_ONLINE_STATUS_1_ONLINE = "1";

}