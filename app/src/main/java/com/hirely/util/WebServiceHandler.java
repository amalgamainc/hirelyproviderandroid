package com.hirely.util;

import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.util.Log;

import com.hirely.GlobalConstants;
import com.hirely.entity.Job;
import com.hirely.entity.Provider;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

@SuppressWarnings("deprecation")
public class WebServiceHandler {


    private final static String TAG = WebServiceHandler.class.getSimpleName();
    public static final String SUCCESS = "success";
    public static final String FAIL = "fail";


    public static String createProviderAccount(Context c, String companyName, String providerName, String password,
                                               String providerEmail, String countryCode, String providerPhone, String deviceId,
                                               String deviceType, String latitude, String longitude, String providerServiceType,
                                               String providerStatus) {

        Log.e(TAG, "CompanyName befor request " + companyName);

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("company_name", companyName)
                    .appendQueryParameter(GlobalConstants.USERNAME, providerName)
                    .appendQueryParameter(GlobalConstants.EMAIL, providerEmail)
                    .appendQueryParameter(GlobalConstants.PASSWORD, password)
                    .appendQueryParameter(GlobalConstants.CODE, countryCode)
                    .appendQueryParameter(GlobalConstants.VERIFY_MOB, providerPhone)
                    .appendQueryParameter(GlobalConstants.DEVICEID, deviceId)
                    .appendQueryParameter(GlobalConstants.DEVICETYPE, deviceType)
                    .appendQueryParameter(GlobalConstants.LATITUDE, latitude)
                    .appendQueryParameter(GlobalConstants.LONGITUDE, longitude)
                    .appendQueryParameter(GlobalConstants.STATUS, providerStatus)
                    .appendQueryParameter(GlobalConstants.SERVICETYPE, providerServiceType)
                    .appendQueryParameter("ProviderREGISTER", "REGISTER");
            String query = builder.build().getEncodedQuery();


            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("success")) {
                PrefUtil.setRegisterMessage(c, job.getString("message"));
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject obj1 = jary.getJSONObject(i);

                    PrefUtil.setCompanyName(c, obj1.getString("company_name"));
                    PrefUtil.setProviderName(c, obj1.getString("username"));
                    PrefUtil.setProviderEmail(c, obj1.getString("email"));
                    PrefUtil.setProviderPhone(c, obj1.getString("mobileno"));
                    PrefUtil.setProviderID(c, obj1.getString("id"));
                    PrefUtil.setProviderStatus(c, obj1.getString("provider_status"));
                    PrefUtil.setServiceType(c, obj1.getString("service_type"));
                    PrefUtil.setProviderPhoneCountryCode(c, obj1.getString("code"));
                }
                return SUCCESS;
            } else {
                PrefUtil.setRegisterMessage(c, job.getString("message"));
                return FAIL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }

    public static String loginWithHirely(Context c, String login_username_mString, String login_password_mString,
                                         String deviceId, String deviceType, String latitude, String longitude,
                                         String usertype_mString) {

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.EMAIL, login_username_mString)
                    .appendQueryParameter(GlobalConstants.passwordprovider, login_password_mString)
                    .appendQueryParameter(GlobalConstants.DEVICEID, deviceId)
                    .appendQueryParameter(GlobalConstants.DEVICETYPE, deviceType)
                    .appendQueryParameter(GlobalConstants.LATITUDE, latitude)
                    .appendQueryParameter(GlobalConstants.LONGITUDE, longitude)
                    .appendQueryParameter(GlobalConstants.USERTYPE, usertype_mString)
                    .appendQueryParameter("LOGIN", "LOGIN");
            String query = builder.build().getEncodedQuery();
            Log.i(TAG, "loginWithHirely query: " + query);
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(

                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            Log.i(TAG, "loginWithHirely response: " + response.toString());

            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("sucessfully login")) {

                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject obj1 = jary.getJSONObject(i);

                    Log.e("User name", "" + obj1.getString("username") + obj1.getString("id"));
                    Log.e("provider status", "" + obj1.getString("provider_status"));

                    PrefUtil.setCompanyName(c, obj1.getString("company_name"));
                    PrefUtil.setProviderName(c, obj1.getString("username"));
                    PrefUtil.setProviderEmail(c, obj1.getString("email"));
                    PrefUtil.setProviderPhone(c, obj1.getString("mobileno"));
                    PrefUtil.setProviderID(c, obj1.getString("id"));
                    PrefUtil.setProviderPrice(c, obj1.getString("price"));
                    PrefUtil.setProviderStatus(c, obj1.getString("provider_status"));
                    PrefUtil.setServiceType(c, obj1.getString("service_type"));
                    PrefUtil.setProviderPhoneCountryCode(c, obj1.getString("code"));
                    PrefUtil.setProviderVerification(c, obj1.getString("verified"));
                }
                return SUCCESS;
            } else {
                PrefUtil.setRegisterMessage(c, job.getString("message"));
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return FAIL;
    }

    public static String loginFacebook(Context context, String providerName, String providerEmail,
                                       String providerSocialId, String deviceId, String deviceType, String latitude,
                                       String longitude, String providerStatus, String serviceType,
                                       String countryCode, String phone, String companyName) {
        String success = "false";

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.SOCIALUSERNAME, providerName)
                    .appendQueryParameter(GlobalConstants.SOCIALEMAIL, providerEmail)
                    .appendQueryParameter(GlobalConstants.SOCIALUNIQUEID, providerSocialId)
                    .appendQueryParameter(GlobalConstants.DEVICEID, deviceId)
                    .appendQueryParameter(GlobalConstants.DEVICETYPE, deviceType)
                    .appendQueryParameter(GlobalConstants.LATITUDE, latitude)
                    .appendQueryParameter(GlobalConstants.LONGITUDE, longitude)
                    .appendQueryParameter(GlobalConstants.STATUS, providerStatus)
                    .appendQueryParameter("service_type", serviceType)
                    .appendQueryParameter("mobileno", phone)
                    .appendQueryParameter("code", countryCode)
                    .appendQueryParameter("company_name", companyName)
                    .appendQueryParameter("Providerfacebook", "Providerfacebook");
            String query = builder.build().getEncodedQuery();

            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("inserted")) {
                success = "true";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject obj1 = jary.getJSONObject(i);

                    PrefUtil.setProviderName(context, obj1.getString("username"));
                    PrefUtil.setProviderEmail(context, obj1.getString("email"));
                    PrefUtil.setProviderPhone(context, obj1.getString("mobileno"));
                    PrefUtil.setProviderID(context, obj1.getString("id"));
                    PrefUtil.setProviderStatus(context, obj1.getString("provider_status"));
                    PrefUtil.setServiceType(context, obj1.getString("service_type"));

                    Log.e("provider status", "" + obj1.getString("provider_status"));
                }
            } else if (status.equalsIgnoreCase("updated")) {
                success = "true";
                JSONArray jary = job.getJSONArray("message");

                for (int i = 0; i < jary.length(); i++) {
                    JSONObject obj1 = jary.getJSONObject(i);

                    PrefUtil.setCompanyName(context, obj1.getString("company_name"));
                    PrefUtil.setProviderName(context, obj1.getString("username"));
                    PrefUtil.setProviderEmail(context, obj1.getString("email"));
                    PrefUtil.setProviderPhone(context, obj1.getString("mobileno"));
                    PrefUtil.setProviderID(context, obj1.getString("id"));
                    PrefUtil.setProviderStatus(context, obj1.getString("provider_status"));
                }
            } else {
                success = "false";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    public static String loginGooglePlus(Context context, String username, String email,
                                         String userId, String deviceId, String deviceType, String lat,
                                         String lon, String userStatus, String serviceType,
                                         String countryCode, String phone, String companyName) {
        String success = "false";

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.SOCIALUSERNAME, username)
                    .appendQueryParameter(GlobalConstants.SOCIALEMAIL, email)
                    .appendQueryParameter(GlobalConstants.SOCIALUNIQUEID, userId)
                    .appendQueryParameter(GlobalConstants.DEVICEID, deviceId)
                    .appendQueryParameter(GlobalConstants.DEVICETYPE, deviceType)
                    .appendQueryParameter(GlobalConstants.LATITUDE, lat)
                    .appendQueryParameter(GlobalConstants.LONGITUDE, lon)
                    .appendQueryParameter(GlobalConstants.STATUS, userStatus)
                    .appendQueryParameter("mobileno", phone)
                    .appendQueryParameter("code", countryCode)
                    .appendQueryParameter("service_type", serviceType)
                    .appendQueryParameter("company_name", companyName)
                    .appendQueryParameter("Providergoogle", "Providergoogle");
            String query = builder.build().getEncodedQuery();
            Log.i(TAG, "loginGooglePlus query: " + query);
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.i(TAG, "loginGooglePlus response: " + response);
            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("inserted")) {
                success = "true";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject obj1 = jary.getJSONObject(i);

                    PrefUtil.setCompanyName(context, obj1.getString("company_name"));
                    PrefUtil.setProviderName(context, obj1.getString("username"));
                    PrefUtil.setProviderEmail(context, obj1.getString("email"));
                    PrefUtil.setProviderPhone(context, obj1.getString("mobileno"));
                    PrefUtil.setProviderID(context, obj1.getString("id"));
                    PrefUtil.setProviderStatus(context, obj1.getString("provider_status"));

                    Log.e("user status", "" + obj1.getString("user_status"));
                }
            } else if (status.equalsIgnoreCase("updated")) {
                success = "true";
                JSONArray jary = job.getJSONArray("message");

                for (int i = 0; i < jary.length(); i++) {
                    JSONObject obj1 = jary.getJSONObject(i);

                    PrefUtil.setProviderName(context, obj1.getString("username"));
                    PrefUtil.setProviderEmail(context, obj1.getString("email"));
                    PrefUtil.setProviderPhone(context, obj1.getString("mobileno"));
                    PrefUtil.setProviderID(context, obj1.getString("id"));
                    PrefUtil.setProviderStatus(context, obj1.getString("provider_status"));

                    Log.e("user status", "" + obj1.getString("user_status"));
                }
            } else {
                success = "false";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    public static String forgetPassword(Context c, String providerEmail, String newPassword) {

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.FORGET_EMAIL, providerEmail)
                    .appendQueryParameter(GlobalConstants.FORGET_PASS, newPassword)
                    .appendQueryParameter("Forgot_password", "Forgot_password");
            String query = builder.build().getEncodedQuery();

            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("updatedddddd")) {
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject obj1 = jary.getJSONObject(i);

                    PrefUtil.setProviderName(c, obj1.getString("username"));
                    PrefUtil.setProviderEmail(c, obj1.getString("email"));
                    PrefUtil.setProviderPhone(c, obj1.getString("mobileno"));
                    PrefUtil.setProviderID(c, obj1.getString("id"));
                    PrefUtil.setProviderStatus(c, obj1.getString("provider_status"));
                    PrefUtil.setProviderPhoneCountryCode(c, obj1.getString("code"));
                    PrefUtil.setServiceType(c, obj1.getString("service_type"));
                }
                return SUCCESS;
            } else {

                PrefUtil.setRegisterMessage(c, job.getString("message"));
                return FAIL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }

    public static String changeProviderPassword(Context context, String currentPassword, String newPassword) {
        String res = "";

        try {
            OkHttpClient client = new OkHttpClient();

            RequestBody formBody = new FormBody.Builder()
                    .add("provider_id", PrefUtil.getProviderID(context))
                    .add("current_password", currentPassword)
                    .add("new_password", newPassword)
                    .add("change_password", "change_password")
                    .build();

            Request request = new Request.Builder()
                    .url(GlobalConstants.url)
                    .post(formBody)
                    .build();

            final Buffer buffer = new Buffer();
            request.body().writeTo(buffer);
            Log.i(TAG, "changeProviderPassword query: " + buffer.readUtf8());

            Response response = client.newCall(request).execute();

            String responseString = response.body().string();
            Log.d(TAG, "changeProviderPassword response: " + responseString);
            JSONObject object = new JSONObject(responseString);
            String status = object.getString("status");
            if (status.equalsIgnoreCase("success")) {
                res = "1";
            } else {
                res = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }





    public static String editProviderProfile(Context c, String companyName, String username, String email, String mobileNumber, String countryCode) {

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("company_name", companyName)
                    .appendQueryParameter("id", PrefUtil.getProviderID(c))
                    .appendQueryParameter("username", username)
                    .appendQueryParameter("email", email)
                    .appendQueryParameter("code", countryCode)
                    .appendQueryParameter("mobileno", mobileNumber)
                    .appendQueryParameter("update_provider_by_id", "update_provider_by_id");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "Edit profile query: " + query);

            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            Log.i(TAG, "Edit profile response: " + response);

            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("updated")) {
                return SUCCESS;
            } else {
                return FAIL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }

    public static Provider providerInformation(Context c) {

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.PROVIDER_ID, PrefUtil.getProviderID(c))
                    .appendQueryParameter("provider_detail", "provider_detail");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "providerInformation query: " + query);

            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            Log.i(TAG, "providerInformation response: " + response);

            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("sucess")) {

                Provider providerInfo = new Provider();

                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject providerJSON = jary.getJSONObject(i);

                    PrefUtil.setCompanyName(c, providerJSON.getString("company_name"));
                    PrefUtil.setProviderName(c, providerJSON.getString("username"));
                    PrefUtil.setProviderEmail(c, providerJSON.getString("email"));
                    PrefUtil.setProviderPhone(c, providerJSON.getString("mobileno"));
                    PrefUtil.setProviderPhoneCountryCode(c, providerJSON.getString("code"));
                    PrefUtil.setProviderID(c, providerJSON.getString("id"));
                    PrefUtil.setProviderStatus(c, providerJSON.getString("provider_status"));
                    PrefUtil.setServiceType(c, providerJSON.getString("service_type"));
                    PrefUtil.setProviderVerification(c, providerJSON.getString("verified"));

                    providerInfo.company = providerJSON.getString("company_name");
                    providerInfo.id = providerJSON.getString("id");
                    providerInfo.name = providerJSON.getString("username");

                    providerInfo.addServicePriceIfNotNull("swedish_60", Integer.parseInt(providerJSON.getString("swedish_60")));
                    providerInfo.addServicePriceIfNotNull("swedish_90", Integer.parseInt(providerJSON.getString("swedish_90")));
                    providerInfo.addServicePriceIfNotNull("swedish_120", Integer.parseInt(providerJSON.getString("swedish_120")));
                    providerInfo.addServicePriceIfNotNull("shiatsu_60", Integer.parseInt(providerJSON.getString("shiatsu_60")));
                    providerInfo.addServicePriceIfNotNull("shiatsu_90", Integer.parseInt(providerJSON.getString("shiatsu_90")));
                    providerInfo.addServicePriceIfNotNull("shiatsu_120", Integer.parseInt(providerJSON.getString("shiatsu_120")));
                    providerInfo.addServicePriceIfNotNull("thai_60", Integer.parseInt(providerJSON.getString("thai_60")));
                    providerInfo.addServicePriceIfNotNull("thai_90", Integer.parseInt(providerJSON.getString("thai_90")));
                    providerInfo.addServicePriceIfNotNull("thai_120", Integer.parseInt(providerJSON.getString("thai_120")));
                    providerInfo.addServicePriceIfNotNull("mixed_60", Integer.parseInt(providerJSON.getString("mixed_60")));
                    providerInfo.addServicePriceIfNotNull("mixed_90", Integer.parseInt(providerJSON.getString("mixed_90")));
                    providerInfo.addServicePriceIfNotNull("mixed_120", Integer.parseInt(providerJSON.getString("mixed_120")));
                    providerInfo.addServicePriceIfNotNull("body scrub", Integer.parseInt(providerJSON.getString("body_scrub_price")));
                    providerInfo.addServicePriceIfNotNull("facial", Integer.parseInt(providerJSON.getString("facial_price")));
                    providerInfo.addServicePriceIfNotNull("foot spa", Integer.parseInt(providerJSON.getString("foot_spa_price")));
                    providerInfo.addServicePriceIfNotNull("pedicure", Integer.parseInt(providerJSON.getString("pedicure_price")));
                    providerInfo.addServicePriceIfNotNull("manicure", Integer.parseInt(providerJSON.getString("manicure_price")));

                    return providerInfo;
                }
            } else {
                PrefUtil.setRegisterMessage(c, job.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean updateProviderLocation(Context c, Location location) {
        boolean isSuccess = false;
        try {
            OkHttpClient client = new OkHttpClient();

            RequestBody formBody = new FormBody.Builder()
                    .add("lat", String.valueOf(location.getLatitude()))
                    .add("lon", String.valueOf(location.getLongitude()))
                    .add("provider_id", PrefUtil.getProviderID(c))
                    .add("update_provider_location", "update_provider_location").build();

            Request request = new Request.Builder()
                    .url(GlobalConstants.url)
                    .post(formBody)
                    .build();

            final Buffer buffer = new Buffer();
            request.body().writeTo(buffer);
            Log.i(TAG, "updateProviderLocation query: " + buffer.readUtf8());

            Response response = client.newCall(request).execute();

            String responseString = response.body().string();
            Log.d(TAG, "updateProviderLocation response: " + responseString);
            JSONObject responseJSON = new JSONObject(responseString);
            isSuccess = responseJSON.getString("status").equals("success");
            if (isSuccess) {
                PrefUtil.setJobDistance(c, responseJSON.getString("distance"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isSuccess;
    }

    public static String updateProviderStatus(Context c, String providerStatus) {

        String result = "";

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.PROID, PrefUtil.getProviderID(c))
                    .appendQueryParameter(GlobalConstants.PROSTATUS, providerStatus)
                    .appendQueryParameter("update_provider_status", "update_provider_status");
            String query = builder.build().getEncodedQuery();
            Log.i(TAG, "updateProviderStatus query: " + query);
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.i(TAG, "updateProviderStatus response: " + response);
            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("updated")) {
                result = "1";
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String updateProviderOnLineStatus (Context context, String onLineStatus) {
        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.PROID, PrefUtil.getProviderID(context))
                    .appendQueryParameter("online_status", onLineStatus)
                    .appendQueryParameter("update_provider_online_status", "update_provider_online_status");
            String query = builder.build().getEncodedQuery();
            Log.i(TAG, "updateProviderOnLineStatus query: " + query);
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.i(TAG, "updateProviderOnLineStatus response: " + response);
            JSONObject job = new JSONObject(response.toString());
            String responseStatus = job.getString("status");
            if (responseStatus.equalsIgnoreCase("updated")) {
                return SUCCESS;
            } else {
                return FAIL;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }

    public static String getProviderTotalEarn (Context c) {

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("id", PrefUtil.getProviderID(c))
                    .appendQueryParameter("get_total_pay", "get_total_pay");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "getProviderTotalEarn query: " + query);

            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            Log.i(TAG, "getProviderTotalEarn response: " + response);

            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("success")) {
                PrefUtil.setEarnedTotal(c, job.getString("total"));
                return SUCCESS;
            } else {
                return FAIL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return FAIL;
    }

    public static String getMoneyForDayAndTotal(Context c) {

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("id", PrefUtil.getProviderID(c))
                    .appendQueryParameter("get_total_pay", "get_total_pay");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "getMoneyForDayAndTotal query: " + query);

            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            Log.i(TAG, "getMoneyForDayAndTotal response: " + response);

            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("success")) {
                PrefUtil.setEarnedToday(c, job.getString("day"));
                PrefUtil.setEarnedTotal(c, job.getString("total"));
                return SUCCESS;
            } else {
                return FAIL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }





    public static String serviceDetails(Context c) {

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.SERVICEID, PrefUtil.getJobServiceId(c))
                    .appendQueryParameter("service_detail", "service_detail");
            String query = builder.build().getEncodedQuery();

            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            Log.i(TAG, "SlideToBegin response: " + response);

            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");

            if (status.equalsIgnoreCase("sucess")) {
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject obj1 = jary.getJSONObject(i);

                    PrefUtil.setJobUserId(c, obj1.getString("user_id"));
                    PrefUtil.setJobAddress(c, obj1.getString("address"));
                    PrefUtil.setServiceType(c, obj1.getString("service_type"));
                    PrefUtil.setJobDistance(c, obj1.getString("distance"));
                    PrefUtil.setJobEmail(c, obj1.getString("email"));
                    PrefUtil.setJobLatitude(c, obj1.getString("lat"));
                    PrefUtil.setJobLongitude(c, obj1.getString("lon"));
                }
                return SUCCESS;
            } else {
                PrefUtil.setRegisterMessage(c, job.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }

    public static List<Job> getRequestedJobList(Context c) {

        List<Job> jobList = new ArrayList<>();

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("provider_id", PrefUtil.getProviderID(c))
                    .appendQueryParameter(GlobalConstants.PROVIDERSERVICETYPE, PrefUtil.getProviderServiceType(c))
                    .appendQueryParameter("requested_jobs", "user");
            String query = builder.build().getEncodedQuery();
            Log.i(TAG, "getRequestedJobList query: " + query);
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.i(TAG, "getRequestedJobList response: " + response);
            JSONObject jobJsonObject = new JSONObject(response.toString());
            String status = jobJsonObject.getString("status");
            if (status.equalsIgnoreCase("success")) {
                JSONArray main = jobJsonObject.getJSONArray("message");
                Job job;
                for (int i = 0; i < main.length(); i++) {
                    JSONObject jobJSON = main.getJSONObject(i);

                    job = new Job();
                    job.jobId = jobJSON.getString("id");
                    job.userId = jobJSON.getString("user_id");
                    job.providerId = jobJSON.getString("provider_id");
                    job.serviceId = jobJSON.getString("service_id");
                    job.address = jobJSON.getString("address");
                    job.distance = jobJSON.getString("distance");
                    job.latitude = jobJSON.getString("lat");
                    job.longitude = jobJSON.getString("lon");
                    job.userName = jobJSON.getString("username");
                    job.massageType = jobJSON.getString("massage_type");
                    Collections.addAll(job.requestedServices, jobJSON.getString("requested_services").split("\n"));

                    jobList.add(job);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage(), e);
        }
        return jobList;
    }

    public static String getUserInfo(Context c) {

        try {
            OkHttpClient client = new OkHttpClient();

            RequestBody formBody = new FormBody.Builder()
                    .add("id", PrefUtil.getJobUserId(c))
                    .add("user", "user")
                    .build();

            Request request = new Request.Builder()
                    .url(GlobalConstants.url)
                    .post(formBody)
                    .build();

            final Buffer buffer = new Buffer();
            request.body().writeTo(buffer);
            Log.i(TAG, "getUserInfo query: " + buffer.readUtf8());

            Response response = client.newCall(request).execute();

            String responseString = response.body().string();
            Log.d(TAG, "getUserInfo response: " + responseString);
            JSONObject object = new JSONObject(responseString);
            JSONArray jsonArray = object.getJSONArray("message");
            String status = object.getString("status");
            if (status.equalsIgnoreCase("success")) {
                JSONObject user = jsonArray.getJSONObject(0);
                PrefUtil.setJobUserUsername(c, user.getString("username"));
                PrefUtil.setJobPhone(c, user.getString("code") + user.getString("mobileno"));
                return SUCCESS;
            } else {
                return FAIL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }

    public static String getUserDetails(Context c, String userId) {

        String username = "";

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("id", userId)
                    .appendQueryParameter("users_detail", "users_detail");
            String query = builder.build().getEncodedQuery();

            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();


            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("sucess")) {
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject obj1 = jary.getJSONObject(i);
                    username = obj1.getString("username");
                    Log.e("INFO ABOUT USER API", "username =" + username);
                }
            } else {
                PrefUtil.setRegisterMessage(c, job.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return username;
    }





    public static String TimerStop(Context c, String time_mString, String cost_mString) {

        String result = "";

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("provider_id", PrefUtil.getProviderID(c))
                    .appendQueryParameter(GlobalConstants.TIME, time_mString)
                    .appendQueryParameter(GlobalConstants.COST, cost_mString)
                    .appendQueryParameter("completed_time", "completed_time");
            String query = builder.build().getEncodedQuery();
            Log.i(TAG, "TimerStop query: " + query);
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.i(TAG, "TimerStop response: " + response);
            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("inserted")) {
                result = "1";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                }
            } else {
                result = "0";
                PrefUtil.setRegisterMessage(c, job.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String sendFeedback(Context c, String serviceId, String comment, float rating) {

        try {
            URL obj = new URL(GlobalConstants.url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("POST");

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.SERVICE_ID, serviceId)
                    .appendQueryParameter(GlobalConstants.COMMENT, comment)
                    .appendQueryParameter(GlobalConstants.RATING, String.valueOf(rating))
                    .appendQueryParameter(GlobalConstants.IS_PROVIDER, "1")
                    .appendQueryParameter("feedback", "feedback");
            String query = builder.build().getEncodedQuery();

            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + obj);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Log.i(TAG, "Response:" + response.toString());
            JSONObject job = new JSONObject(response.toString());
            String status = job.getString("status");
            if (status.equalsIgnoreCase("success")) {
                return SUCCESS;
            } else {
                PrefUtil.setRegisterMessage(c, job.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }





    public static String acceptJob(Context context, String requestedServices) {

        String res = "";

        try {
            OkHttpClient client = new OkHttpClient();

            RequestBody formBody = new FormBody.Builder()
                    .add("user_id", PrefUtil.getJobUserId(context))
                    .add("provider_id", PrefUtil.getProviderID(context))
                    .add("service_type", PrefUtil.getProviderServiceType(context))
                    .add("requested_services", requestedServices)
                    .add("accept_job", "accept_job")
                    .build();

            Request request = new Request.Builder()
                    .url(GlobalConstants.url)
                    .post(formBody)
                    .build();

            final Buffer buffer = new Buffer();
            request.body().writeTo(buffer);
            Log.i(TAG, "acceptJob query: " + buffer.readUtf8());

            Response response = client.newCall(request).execute();

            String responseString = response.body().string();
            Log.d(TAG, "accept_job response: " + responseString);
            JSONObject object = new JSONObject(responseString);
            String status = object.getString("status");
            if (status.equalsIgnoreCase("success")) {
                res = object.getString("message");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    public static String declineJob(Context context) {

        try {
            OkHttpClient client = new OkHttpClient();

            RequestBody formBody = new FormBody.Builder()
                    .add("user_id", PrefUtil.getJobUserId(context))
                    .add("decline_job", "decline_job")
                    .build();

            Request request = new Request.Builder()
                    .url(GlobalConstants.url)
                    .post(formBody)
                    .build();

            final Buffer buffer = new Buffer();
            request.body().writeTo(buffer);
            Log.i(TAG, "declineJob query: " + buffer.readUtf8());

            Response response = client.newCall(request).execute();

            String responseString = response.body().string();
            Log.d(TAG, "declineJob response: " + responseString);
            JSONObject object = new JSONObject(responseString);
            String status = object.getString("status");
            if (status.equalsIgnoreCase("success")) {
                return SUCCESS;
            } else {
                return FAIL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }

    public static String startJob(Context context) {

        try {
            OkHttpClient client = new OkHttpClient();

            RequestBody formBody = new FormBody.Builder()
                    .add("user_id", PrefUtil.getJobUserId(context))
                    .add("start_job", "start_job")
                    .build();

            Request request = new Request.Builder()
                    .url(GlobalConstants.url)
                    .post(formBody)
                    .build();

            final Buffer buffer = new Buffer();
            request.body().writeTo(buffer);
            Log.i(TAG, "startJob query: " + buffer.readUtf8());

            Response response = client.newCall(request).execute();

            String responseString = response.body().string();
            Log.d(TAG, "startJob response: " + responseString);
            JSONObject object = new JSONObject(responseString);
            String status = object.getString("status");
            if (status.equalsIgnoreCase("success")) {
                return SUCCESS;
            } else {
                return FAIL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }

    public static String finishJob(Context context, String price) {

        try {
            OkHttpClient client = new OkHttpClient();

            RequestBody formBody = new FormBody.Builder()
                    .add("user_id", PrefUtil.getJobUserId(context))
                    .add("provider_id", PrefUtil.getProviderID(context))
                    .add("price", price)
                    .add("finish_job", "finish_job")
                    .build();

            Request request = new Request.Builder()
                    .url(GlobalConstants.url)
                    .post(formBody)
                    .build();

            final Buffer buffer = new Buffer();
            request.body().writeTo(buffer);
            Log.i(TAG, "finishJob query: " + buffer.readUtf8());

            Response response = client.newCall(request).execute();

            String responseString = response.body().string();
            Log.d(TAG, "finishJob response: " + responseString);
            JSONObject object = new JSONObject(responseString);
            String status = object.getString("status");
            if (status.equalsIgnoreCase("success")) {
                return SUCCESS;
            } else {
                return FAIL;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAIL;
    }

}