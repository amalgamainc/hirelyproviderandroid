package com.hirely.util;

import java.util.regex.Pattern;

public class ValidationUtil {
    private static final String USERNAME_PATTERN = "[a-zA-Z ]+";
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String MOBILE_PATTERN = "\\d+";

    private ValidationUtil() {
    }

    public static boolean isUsernameValid(String username) {
        return !username.isEmpty();
    }

    public static boolean isEmailValid(String email) {
        return Pattern.compile(EMAIL_PATTERN).matcher(email).matches();
    }

    public static boolean isPasswordValid(String pass) {
        return pass != null && pass.length() > 6;
    }

    public static boolean isMobilePhoneValid(String phone) {
        return Pattern.compile(MOBILE_PATTERN).matcher(phone).matches();
    }
}
