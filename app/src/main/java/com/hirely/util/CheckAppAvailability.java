package com.hirely.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.provider.Settings;

/**
 * Created in GET-IT by chornenkyy@gmail.com on 25.02.2016.
 */
public class CheckAppAvailability {
    private static final String MSG_NO_GPS = "Your GPS seems to be disabled, do you want to enable it?";
    private static final String MSG_NO_INTERNET = "Your internet connection seems to be disabled, do you want to enable it?";

    private CheckAppAvailability() {
    }

    public static boolean isInternetOn(Context c) {

//        // get Connectivity Manager object to check connection
//        ConnectivityManager connec =
//                (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
//
//        // Check for network connections
//        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
//                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
//                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
//                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {
//
//            return true;
//
//        } else if (
//                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
//                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {
//
//            return false;
//        }
//        return false;

        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static boolean isGPSOn(Context c) {
        final LocationManager manager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static void showMessage(final Activity activity, final Type type) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(getMessage(type))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        activity.startActivity(new Intent(getAction(type)));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        System.exit(0);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private static String getMessage(Type type) {
        switch (type) {
            case INTERNET:
                return MSG_NO_INTERNET;
            case GPS:
                return MSG_NO_GPS;
            default:
                return null;
        }
    }

    private static String getAction(Type type) {
        switch (type) {
            case INTERNET:
                return Settings.ACTION_SETTINGS;
            case GPS:
                return Settings.ACTION_LOCATION_SOURCE_SETTINGS;
            default:
                return null;
        }
    }

    public enum Type {
        INTERNET, GPS
    }
}
