package com.hirely.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefUtil {

    private PrefUtil() {
    }

    private static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setProviderName(Context c, String name) {
        getPrefs(c).edit().putString("providerName", name).apply();
    }

    public static String getProviderName(Context c) {
        return getPrefs(c).getString("providerName", null);
    }

    public static void setProviderEmail(Context c, String email) {
        getPrefs(c).edit().putString("providerEmail", email).apply();
    }

    public static String getProviderEmail(Context c) {
        return getPrefs(c).getString("providerEmail", null);
    }

    public static void setProviderPhone(Context c, String phone) {
        getPrefs(c).edit().putString("providerPhone", phone).apply();
    }

    public static String getProviderPhone(Context c) {
        return getPrefs(c).getString("providerPhone", null);
    }

    public static void setProviderID(Context c, String id) {
        getPrefs(c).edit().putString("providerId", id).apply();
    }

    public static String getProviderID(Context c) {
        return getPrefs(c).getString("providerId", null);
    }

    public static void setProviderStatus(Context c, String providerStatus) {
        getPrefs(c).edit().putString("providerInformation", providerStatus).apply();
    }

    public static String getProviderStatus(Context c) {
        return getPrefs(c).getString("providerInformation", null);
    }

    public static void setServiceType(Context c, String serviceType) {
        getPrefs(c).edit().putString("providerServiceType", serviceType).apply();
    }

    public static String getProviderServiceType(Context c) {
        return getPrefs(c).getString("providerServiceType", null);
    }

    public static void setProviderPhoneCountryCode(Context c, String phoneCountryCode) {
        getPrefs(c).edit().putString("providerPhoneCountryCode", phoneCountryCode).apply();
    }

    public static String getProviderPhoneCountryCode(Context c) {
        return getPrefs(c).getString("providerPhoneCountryCode", null);
    }

    public static void setProviderLatitude(Context c, float latitude) {
        getPrefs(c).edit().putFloat("providerLatitude", latitude).apply();
    }

    public static double getProviderLatitude(Context c) {
        return getPrefs(c).getFloat("providerLatitude", 0);
    }

    public static void setProviderLongitude(Context c, float longitude) {
        getPrefs(c).edit().putFloat("providerLongitude", longitude).apply();
    }

    public static double getProviderLongitude(Context c) {
        return getPrefs(c).getFloat("providerLongitude", 0);
    }

    public static void setProviderPrice(Context c, String price) {
        getPrefs(c).edit().putString("providerPrice", price).apply();
    }

    public static String getProviderPrice(Context c) {
        return getPrefs(c).getString("providerPrice", null);
    }

    public static void setRegisterMessage(Context c, String message) {
        getPrefs(c).edit().putString("registerMsg", message).apply();
    }

    public static String getRegisterMessage(Context c) {
        return getPrefs(c).getString("registerMsg", null);
    }

    public static void setJobUserId(Context c, String userId) {
        getPrefs(c).edit().putString("jobUserId", userId).apply();
    }

    public static String getJobUserId(Context c) {
        return getPrefs(c).getString("jobUserId", null);
    }

    public static void setJobAddress(Context c, String address) {
        getPrefs(c).edit().putString("jobAddress", address).apply();
    }

    public static String getJobAddress(Context c) {
        return getPrefs(c).getString("jobAddress", null);
    }

    public static void setJobPhone(Context c, String phone) {
        getPrefs(c).edit().putString("jobPhone", phone).apply();
    }

    public static String getJobPhone(Context c) {
        return getPrefs(c).getString("jobPhone", null);
    }

    public static void setJobDistance(Context c, String distance) {
        getPrefs(c).edit().putString("jobDistance", distance).apply();
    }

    public static String getJobDistance(Context c) {
        return getPrefs(c).getString("jobDistance", null);
    }

    public static void setJobEmail(Context c, String email) {
        getPrefs(c).edit().putString("jobEmail", email).apply();
    }

    public static String getJobEmail(Context c) {
        return getPrefs(c).getString("jobEmail", null);
    }

    public static void setJobLatitude(Context c, String latitude) {
        getPrefs(c).edit().putString("jobLatitude", latitude).apply();
    }

    public static String getJobLatitude(Context c) {
        return getPrefs(c).getString("jobLatitude", null);
    }

    public static void setJobLongitude(Context c, String longitude) {
        getPrefs(c).edit().putString("jobLongitude", longitude).apply();
    }

    public static String getJobLongitude(Context c) {
        return getPrefs(c).getString("jobLongitude", null);
    }

    public static void setJobServiceId(Context c, String serviceId) {
        getPrefs(c).edit().putString("jobServiceID", serviceId).apply();
    }

    public static String getJobServiceId(Context c) {
        return getPrefs(c).getString("jobServiceID", null);
    }

    public static void setDeviceID(Context c, String deviceID) {
        getPrefs(c).edit().putString("deviceID", deviceID).apply();
    }

    public static String getDeviceID(Context c) {
        return getPrefs(c).getString("deviceID", null);
    }

    public static void setAppVersion(Context c, int appVersion) {
        getPrefs(c).edit().putInt("appVersion", appVersion).apply();
    }

    public static int getAppVersion(Context c) {
        return getPrefs(c).getInt("appVersion", 0);
    }

    // SOCIAL NETWORK LOGIN
    public static void setSocialLoginType(Context c, String type) {
        getPrefs(c).edit().putString("loginType", type).apply();
    }

    public static String getSocialLoginType(Context c) {
        return getPrefs(c).getString("loginType", null);
    }

    //// FACEBOOK
    public static void setFacebookId(Context c, String id) {
        getPrefs(c).edit().putString("facebookID", id).apply();
    }

    public static String getFacebookId(Context c) {
        return getPrefs(c).getString("facebookID", null);
    }

    public static void setFacebookEmail(Context c, String email) {
        getPrefs(c).edit().putString("facebookEmail", email).apply();
    }

    public static String getFacebookEmail(Context c) {
        return getPrefs(c).getString("facebookEmail", null);
    }

    public static void setFacebookName(Context c, String name) {
        getPrefs(c).edit().putString("facebookName", name).apply();
    }

    public static String getFacebookName(Context c) {
        return getPrefs(c).getString("facebookName", null);
    }

    public static void setFacebookStatus(Context c, boolean status) {
        getPrefs(c).edit().putBoolean("facebookStatus", status).apply();
    }

    public static boolean getFacebookStatus(Context c) {
        return getPrefs(c).getBoolean("facebookStatus", false);
    }

    //// GOOGLE
    public static void setGoogleId(Context c, String id) {
        getPrefs(c).edit().putString("facebookID", id).apply();
    }

    public static String getGoogleId(Context c) {
        return getPrefs(c).getString("facebookID", null);
    }

    public static void setGoogleEmail(Context c, String email) {
        getPrefs(c).edit().putString("facebookEmail", email).apply();
    }

    public static String getGoogleEmail(Context c) {
        return getPrefs(c).getString("facebookEmail", null);
    }

    public static void setGoogleName(Context c, String name) {
        getPrefs(c).edit().putString("facebookName", name).apply();
    }

    public static String getGoogleName(Context c) {
        return getPrefs(c).getString("facebookName", null);
    }

    public static void setGoogleStatus(Context c, boolean status) {
        getPrefs(c).edit().putBoolean("googleStatus", status).apply();
    }

    public static boolean getGoogleStatus(Context c) {
        return getPrefs(c).getBoolean("googleStatus", false);
    }

    public static void setAppInBackground(Context c, boolean isInBackground) {
        getPrefs(c).edit().putBoolean("appInBackground", isInBackground).apply();
    }

    public static boolean getAppInBackround(Context c) {
        return getPrefs(c).getBoolean("appInBackground", false);
    }

    public static void setLastNotificationID(Context c, int notificationID) {
        getPrefs(c).edit().putInt("lastNotificationID", notificationID).apply();
    }

    public static int getLastNotificationID(Context c) {
        return getPrefs(c).getInt("lastNotificationID", 0);
    }

    public static void setJobPrice(Context c, String priceForWork) {
        getPrefs(c).edit().putString("priceForWork", priceForWork).apply();
    }

    public static String getJobPrice(Context c) {
        return getPrefs(c).getString("priceForWork", null);
    }

    public static void setProviderVerification(Context c, String verified) {
        getPrefs(c).edit().putString("providerVerified", verified).apply();
    }

    public static String getProviderVerification(Context c) {
        return getPrefs(c).getString("providerVerified", null);
    }

    public static void setEarnedToday(Context c, String earnedToday) {
        getPrefs(c).edit().putString("earnedToday", earnedToday).apply();
    }

    public static String getEarnedToday(Context c) {
        return getPrefs(c).getString("earnedToday", null);
    }

    public static void setEarnedTotal(Context c, String earnedTotal) {
        getPrefs(c).edit().putString("earnedTotal", earnedTotal).apply();
    }

    public static String getEarnedTotal(Context c) {
        return getPrefs(c).getString("earnedTotal", null);
    }

    public static void setCompanyName(Context context, String companyName) {
        getPrefs(context).edit().putString("companyName", companyName).apply();
    }

    public static String getCompanyName(Context context) {
        return getPrefs(context).getString("companyName", null);
    }

    public static void setJobUserUsername(Context context, String jobUserUsername) {
        getPrefs(context).edit().putString("jobUserUsername", jobUserUsername).apply();
    }

    public static String getJobUserUsername(Context context) {
        return getPrefs(context).getString("jobUserUsername", null);
    }

    public static void setServiceList(Context context, String serviceList) {
        getPrefs(context).edit().putString("serviceList", serviceList).apply();
    }

    public static String getServiceList(Context context) {
        return getPrefs(context).getString("serviceList", null);
    }

    public static void setJobCost(Context context, String jobCost) {
        getPrefs(context).edit().putString("jobCost", jobCost).apply();
    }

    public static String getJobCost(Context context) {
        return getPrefs(context).getString("jobCost", null);
    }
}