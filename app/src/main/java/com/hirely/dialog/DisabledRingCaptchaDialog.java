package com.hirely.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.hirelyprovider.app.R;

public abstract class DisabledRingCaptchaDialog {

    Context context;
    Dialog dialog;
    String errorMessage;

    public DisabledRingCaptchaDialog(Context context, String errorMessage) {
        this.context = context;
        dialog = new Dialog(context);
        this.errorMessage = errorMessage;
    }

    public void show(){
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_captcha_disabled);

        ((TextView)dialog.findViewById(R.id.errorMessage)).setText(errorMessage);
        Button goBackBtn = (Button) dialog.findViewById(R.id.goback_btn);
        goBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                goBack();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public abstract void goBack();
}
