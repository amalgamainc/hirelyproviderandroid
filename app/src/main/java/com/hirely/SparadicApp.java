package com.hirely;

import android.app.Application;

import com.hirelyprovider.app.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Softensy Android Dev on 08.08.2016.
 */
public class SparadicApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/opensans.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
